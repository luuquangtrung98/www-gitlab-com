title: BI Worldwide
file_name: bi_worldwide
cover_image: /images/blogimages/bi_worldwise_casestudy_image.png
cover_title: |
  Increasing deployments to 10 times a day
cover_description: |
  BI WORLDWIDE Corporate Product Development Team removed technology barriers to focus on building microservices
twitter_image: /images/blogimages/bi_worldwise_casestudy_image.png
canonical_path: /customers/bi_worldwide/
customer_logo: /images/case_study_logos/BI_Worldwide_case_study_logo.svg
customer_logo_css_class: brand-logo-tall
customer_industry: Consulting
customer_location: Minneapolis, Minnesota
customer_solution: Ultimate
customer_employees: 1,001-5,000
customer_overview: >-
    BI WORLDWIDE is a global engagement agency that modernizes and automates their software delivery workflow with a single solution for their complete DevOps lifecycle.
customer_challenge: >-
   The BI WORLDWIDE Corporate Product Development Team faced challenges around modernizing tools, processes, and architecture

key_benefits: >-

    Increased developers' efficiencies by alleviating challenges of  multiple tools, logins, and user experiences

    
    Enhanced automated testing, builds, and deployments

   
    Automated security scans of cloud-deployed microservices as well as on-prem applications with SAST and DAST capabilities.

    
    Completed 56955 builds in the first 11 months of using GitLab


customer_stats:
  - stat:  10x
    label: Daily deployments for already-modernized applications
  - stat: From 4 to 15
    label: Services pushed per release
  - stat:  178
    label: builds per day

customer_study_content:
  - title: the customer
    subtitle: Inspiring employees, sales, channel partners and customers
    content: >-

        BI WORLDWIDE (BIW) inspires people and delivers measurable business results. Inspired employees solve problems and create value.  Inspired salespeople take risks and seize opportunity. And inspired channel partners and customers choose your brand every time. We work with great companies around the globe who know that extraordinary results can only be achieved when their business is energized by the people who make it happen.



  - title: the challenge
    subtitle: Updating legacy systems and moving to multi-tenant configuration
    content: >-

        The BIW Corporate Product Development Team (CPD) was dealing with a large legacy
        codebase that was created years ago. They were looking to eliminate (or at least significantly reduce) manual
        testing and manual deployments to their on-premise infrastructure. They had maintenance backlogs, inefficiencies, and
        cross-organization visibility issues. The goal was to achieve operational efficiency by removing toolchain complexity.

        
        The company was also trying to update the previous architecture of a single-tenant code-change-driven solution and upgrade
        to a multi-tenant configuration-driven solution. The legacy application installation pattern was to install from a fork that
        could (and would) be tweaked to a customer’s needs before deploying to the VMs. This pattern provided flexibility for customers
        but was inefficient and was time-consuming to maintain and upgrade.

        
        “It was entirely time-consuming to apply all of those code changes,” said Adam Dehnel, product architect, BI WORLDWIDE. “As a result,
        we didn’t release as frequently because the application of a new release to those disparate installations was quite a process. That would
        cause us to try to get ‘all the features’ into each release which would then slow the process down more.”

  - title: the solution
    subtitle: Increasing development speed
    content: >-

        The Corporate Product Development Team considered Git source code management while using CVS in 2014. At that time they achieved 9-month release
        cycles with another few months for the code to be distributed to the customer installations for the upgrade. This tempo impacted their pace of
        delivering to market and ability to respond to customer requests.

        
        To help combat this pattern, the product architecture team was given the directive to rethink processes - essentially update and modernize everything.
        They had practices and tools in place at the time but were spending time on items that weren’t business differentiating features. They faced classic issues
        surrounding a lack of cross-team communication including inefficient mechanisms for intra-organization workflows and individualized toolsets.

        
        One of the early moves to help solve these challenges was to migrate from CVS to Git for source control management. They experienced a self-described “painful transition to Git”
        because of the modernization efforts this move required. Once the initial move to GitHub was complete BIW began looking to automate the build, test and deployment process.
        As the first implementation of this, the team built a chain of tools with GitHub talking to Jenkins and then Checkmarx + WebInspect plus JIRA and Confluence for the setup to manage
        the full lifecycle microservices. While these tools were heading in the right direction, the organization was still facing challenges with toolchain complexity and experienced lost bug
        reports, constant context shifting and tool differences throughout the organization.



  - title: the results
    subtitle: Improving efficiency by moving to a single tool
    content: >-

        The CPD team evaluated GitLab’s capabilities against their current toolset and against the competition and selected GitLab to help them achieve their next iteration. The group started
        looking at what tools they could remove to simplify their toolchain. The migration from their previous Git tools to GitLab was painless and they saw an immediate improvement of collaboration
        and release pace as a result of their move to GitLab.

        
        The development, QA, UX and DevOps teams within CPD at BIW have been using GitLab since 2017. In June 2018, additional development groups started to move over to be able to
        easily track all features, stories, requirements and use cases in GitLab Issues. The next goal is to include ongoing bug tracking and expand GitLab to several new internal teams that will
        use it as a collaboration hub for future feature development requests and discussions.

        
        “GitLab is currently running on a Rancher cluster and we’re using AWS Aurora Postgres + Elasticsearch to run it.  And then we just recently migrated from a (different) Rancher cluster
        for our microservices to Amazon EKS and are also pushing our web applications to CloudFront,” Dehnel explains. “All of this deployment is running from GL pipelines.  We are solidly in a
        good CI workflow with some applications in Continuous Delivery.  Heading into 2020 we’re starting to push towards Continuous Deployment for all microservices and microexperiences. We hope to
        leverage some of the GitLab Kubernetes integrations and enhancements to make this transition less work and more consistent.”

        
        The company recently started using GitLab Pipelines to scan (SAST + Dependency initially, but they plan on using DAST capabilities soon) and deploy legacy monolith installations for anyone
        that is running their install in AWS.  Between this setup and the microservices running in GitLab pipelines the first four days of SAST/Dependency scanning the team performed over 300 scans.
        This scanning helped the team identify previously unidentified vulnerabilities and allowed them to rectify the situation going forward. GitLab’s pipelines allow everyone to use the same tooling
        for application scanning and deployment regardless of whether it is legacy or modernized.

        
        While Dehnel admits that they are still using some classic methods, they now have teams that experience releases almost daily-- down from the original pre-Git pace of a release every 9-12 months.
        BIW is using GitLab pipelines to deploy the legacy monolith to AWS.  Within those pipelines, they are performing automated infrastructure provisioning, infrastructure as code, automated
        security tests, and application deployments.

        
        The ultimate win for the company is that the corporate product development team is all working in the same place. The operations infrastructure team is now working from the same platform and
        using the same product for their software development lifecycle. The team is pushing all of their automation tools to the same place instead of working around a daisy chain of tools anymore.
customer_study_quotes:
  - blockquote: One tool for SCM+CI/CD was a big initial win. Now wrapping security scans into that tool as well has already increased our visibility into security vulnerabilities. The integrated Docker registry has also been very helpful for us. Issue/Product management features let everyone operate in the same space regardless of role.
    attribution: Adam Dehnel 
    attribution_title: product architect, BI WORLDWIDE

