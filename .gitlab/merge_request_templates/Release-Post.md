<!--
### Notes for the Release post manager

You'll want to find and replace the following strings:
`@release_post_manager` -> @ mention handle
`@tw_lead` -> @ mention handle
`@tech_advisor` -> @ mention handle
`@pmm_lead` -> @ mention handle
X-Y -> 14-1
X_Y -> 14_1
X.Y -> 14.1
YYYY-MM-DD -> 2021-10-22
YYYY/MM/DD -> 2021/10/22
YYYY-MM -> 2021-10

TODO signals you need to update something

You'll see various expandable sections that have titles like this:
**Due date: YYYY-MM-DD** (20th)
Go through your release date month calendar and figure out what dates the items will happen, the date in parentheses is the target date this should be completed by.
-->

### QUICK JUMP TO A SECTION:

[[_TOC_]]

---

### Overview

Process Improvements? Have suggestions for improving the release post process as we go?
Capture them in the [Retrospective issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/TODO).

- **Preview page** (shows the latest merged content blocks, for reference use untill the 17th): https://about.gitlab.com/releases/gitlab-com/
- **View App** (shows the introduction, MVP and latest merged content blocks, for reference use between 18th - 21st: https://release-X-Y.about.gitlab-review.app/releases/YYYY/MM/DD/gitlab-X-Y-released/index.html


_Release post:_

- **Intro**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/sites/uncategorized/source/releases/posts/YYYY-MM-22-gitlab-X-Y-released.html.md
- **Items**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/data/release_posts/X_Y
- **Images**: https://gitlab.com/gitlab-com/www-gitlab-com/tree/release-X-Y/source/images/X_Y

_Related MRs:_
- **Bug Fixes:** TODO Link MR for bug fixes here
- **Usability improvements:** TODO Link MR for usability improvements here
- **Performance improvements:** TODO Link MR for performance improvements here

_Related files:_

- **Features YAML** link: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/data/features.yml
- **Features YAML Images** link: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/source/images/feature_page/screenshots
- **Homepage card**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/sites/uncategorized/source/includes/home/ten-oh-announcement.html.haml
- **MVPs**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/data/mvps.yml

_Release post branch ownership:_
- The **Release Post Manager** is solely in charge of changes to the release post branch. To avoid potential merge conflicts later during content assembly, it is imperative that Technical Writers do not merge updates from `master` to the release post branch even if it is falling behind or if there is a conflict. The Release Post Manager will take care of conflicts as part of the content assembly process on the 18th and work with the Technical Advisor as needed.

_Handbook references:_

- Blog handbook: https://about.gitlab.com/handbook/marketing/blog/
- Release post handbook: https://about.gitlab.com/handbook/marketing/blog/release-posts/
- Markdown guide: https://about.gitlab.com/handbook/engineering/technical-writing/markdown-guide/

_People:_

- Release Post Managers: https://about.gitlab.com/handbook/marketing/blog/release-posts/managers/
- Release Managers: https://about.gitlab.com/community/release-managers/

| Release post manager | Tech writer | Technical Advisor | Social | PMM lead | Product Operations DRI |
| --- | --- | --- | --- | --- | --- |
| `@release_post_manager` | `@tw_lead` | `@tech_advisor` | DRI: `@wspillane` & `@social` for Slack Checklist item | `@pmm_lead` | `@fseifoddini` |

---

### Release post kickoff (`@release_post_manager`)

 Due date: YYYY-MM-DD (By the 7th)


**Before starting on this checklist, you should have created the release post branch and required files [as explained in the Handbook](https://about.gitlab.com/handbook/marketing/blog/release-posts/#create-your-release-post-branch-and-required-directoriesfiles)**

**Note:** Throughout the release post process, you'll do various Slack reminders/announcements. It is recommended you cc the Product Operations DRI and the rest of your release post team as you do these Slack posts because it helps keep everyone on the same page.

- [ ] Read the [RPM specific section](https://about.gitlab.com/handbook/marketing/blog/release-posts/#release-post-manager-responsibilities) of the release post handbook page in detail
- [ ] Review the rest of the [release post handbook page](https://about.gitlab.com/handbook/marketing/blog/release-posts/) and all the tasks in this MR template, not just your own, so you're familiar with the overall process, roles and tasks involved
- [ ] In `#X-Y-release-post-prep` ask the TW Lead and Technical Advisor to familiarize themselves with their role by reading their section of the release post handbook page.
  - Link to their handbook sections: [TW Lead](https://about.gitlab.com/handbook/marketing/blog/release-posts/#tw-lead) and [TA](https://about.gitlab.com/handbook/marketing/blog/release-posts/#tw-lead)
  - Tell them that if they have any questions, post them to you in the `#X-Y-release-post-prep` Slack channel and cc Product Operations
  - Tell them it's recommended they set up a coffee chat with their predecessor for any helpful tips. The previous TW lead and TA can be found on the [RP scheduling page](https://about.gitlab.com/handbook/marketing/blog/release-posts/managers/)
- [ ] Prior to your first team standup consider setting up a coffee chat with the previous release post manager and/or Product Operations to ask for tips and any helpful "latest info"
- [ ] After meeting with the previous release post manager and/or Product Operations for insights, consider setting up a meeting with your release post shadow to help them understand their role and how much capacity they have to support the work that month
- [ ] Verify this MR is labeled ~"blog post" ~release ~"release post" ~"priority::1" and assigned to you (the Release Post Manager)
- [ ] Add the current milestone to this MR
- [ ] Create a release post retrospective issue by using the [Release post retrospective template](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/.gitlab/issue_templates/Release-Post-Retrospective.md), and use `Release Post X.Y Retrospective` as a title. Once created, update the link at the top of this MR description. [Example retro issue](hhttps://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11011).
  - [ ] Schedule a 50 min Live Retrospective meeting for some time after the 22nd. All action items for the retro need to be completed prior to the 1st of the next month in order to incorporate any process changes before the next release begins. Make sure to invite Product Ops to the Live Retro meeting. Product Ops will need to approve any major updates to the process identified during the Retrospective.
- [ ] Replace each `@mention` in this MR description with the names of the Release Post Manager, Tech Writer, and Social Lead for this release
- [ ] Update the links in this MR description
- [ ] Update all due dates in this MR description
- [ ] Make sure the release post branch has all initial files: `sites/uncategorized/source/releases/posts/YYYY-MM-DD-gitlab-X-Y-released.html.md`, `data/release_posts/X_Y/mvp.yml` and `data/release_posts/X_Y/cta.yml`
- [ ] Add the release number to `sites/uncategorized/source/releases/posts/YYYY-MM-22-gitlab-X-Y-release.html.md` and make sure to remove the backticks.
- [ ] Add your name as the author to `sites/uncategorized/source/releases/posts/YYYY-MM-22-gitlab-X-Y-release.html.md`
- [ ] Update the "which includes our `YY` release kickoff video" line replacing the `YY` (including removing the backticks) to reference the **next** release in `sites/uncategorized/source/releases/posts/YYYY-MM-22-gitlab-X-Y-release.html.md`
- [ ] Per guidance on [communication](https://about.gitlab.com/handbook/marketing/blog/release-posts/#communication) for the release, create the `X-Y-release-post-prep` channel in Slack and invite `@tw_lead`, `@tech_advisor`, `@pmm_lead`, the Product Operations DRI and the release post manager shadow (whoever is running the next release post). Let them know this channel is to discuss production-specific topics that don't concern the broader team, so we can keep `#release-post` focused and easy to follow. As a topic, add the release post MR, the link to the review app, and the link of the retro issue:
- [ ] Update the `#X-Y-release-post-prep` Slack channel topic and slack bookmarks in the `#release-post` channel:

  ```
  MR: TODO https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/1234
  Preview page: https://about.gitlab.com/releases/gitlab-com/
  Review App: https://release-X-Y.about.gitlab-review.app/releases/YYYY/MM/DD/gitlab-X-Y-released/
  Retro issue: TODO https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/1234
  MVP nomination: TODO https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/1234
  ```
  - [ ] Update the `#release-post` Slack channel topic and slack bookmarks in the #release-post channel:

  ```
  MR: TODO https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/1234
  Preview page: https://about.gitlab.com/releases/gitlab-com/
  Review App: https://release-X-Y.about.gitlab-review.app/releases/YYYY/MM/DD/gitlab-X-Y-released/
  MVP nomination: TODO https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/1234
  ```
- [ ] Announce yourself as the Release Post Manager in the `#release-post` channel, as well as the Technical Advisor, and TW lead for reference. cc the Product Operations DRI
- [ ] Set up a 15-minute weekly standup with the Technical Writer, Tech advisor as required attendees and PMM lead as optional, to touch base and troubleshoot. Invite the Product Operations DRI as well, as they will be your backup on all tasks should complications arise. If times zones conflict, this is not mandatory. Sample [agenda](https://docs.google.com/document/d/1LhAEBgZmSq8gs7Dm60RwqU60EZK5cafvILgQUoiakBM/edit#)
- [ ] In the #release-post Slack channel, remind Product Managers that all [content blocks (features, recurring, bugs, etc.)](#content-blocks) should be drafted and under review by the 10th. All direction items and notable community contributions should be included in the release post.
- [ ] Confirm your local dev environment is running a current version of Ruby. See Handbook section [Local dev environment setup to run content assembly script](https://about.gitlab.com/handbook/marketing/blog/release-posts/#local-dev-environment-setup-to-run-content-assembly-script). Not applicable if you used [Gitpod](https://www.gitpod.io/)
- [ ] Remind the Technical Writer (either via slack or weekly standup) not to merge in changes from `master` to the release post branch. See the `Release post branch ownership` section above for more details.

### Release post item creation reminders (`@release_post_manager`)

 **Due date: YYYY-MM-DD** (By the 10th)

**Note:** Throughout the release post-process, you'll do various Slack reminders/announcements. It is recommended you cc the Product Operations DRI and the rest of your release post team as you do these Slack posts because it helps keep everyone on the same page.

- [ ] Remind the product managers in the #release-post channel that today is the day to have Release Post Items created and in review by the PMM and TW counterparts.
- [ ] Remind the product managers in the #release-post channel that it's important we all follow the [file naming guidelines](https://about.gitlab.com/handbook/marketing/blog/release-posts/#instructions) as it affects the release post template as well as the release post team's ability to find release post items with ease.
- [ ] Create the [bugs, usability improvements, and performance improvements MRs](https://about.gitlab.com/handbook/marketing/blog/release-posts/#create-mrs-for-usability-improvements-bugs-and-performance-improvements)
  - [ ] Link these MRs to the top section of this MR so that they are easy to access
  - [ ] Inform EMs `Your notable bugs and performance improvement MRs are ready to receive your contributions.` and link directly the two MRs that you have created. Place this as a comment mentioning `@gitlab-com/backend-managers` and `@gitlab-org/frontend/frontend-managers` in this MR.
  - [ ] Copy the comment and links from the above comment in this MR and share it in the `#development` and `#eng-managers` Slack channels."
  - [ ] Copy the notification from the above comment in this MR and share it in the `#development` and `#eng-managers` Slack channels.
  - [ ] In the `#release-post` channel, share the link to just the Bug Fixes MR and ask PMs to work with their EM to add high impact bugs to the MR
  - [ ] In the `#release-post` channel, share the link to just the Usability Improvements MR and ask PMs to work in partnership with their Product Designers to add line items that highlight significant usability improvements.
  - [ ] In the `#ux_managers` channel, inform them that `Your notable usability improvement MR is ready to receive your contributions. Please work with your Product Manager to add entries. Remember that we have to limit the content block to [notable improvements](https://about.gitlab.com/handbook/marketing/blog/release-posts/#contributing-to-usability-improvements) but you can add as many items as you'd like to the [UI Polish gallery](https://nicolasdular.gitlab.io/gitlab-polish-gallery/)` and include a direct link to the MR.

---

### Recurring items starting on the 12th: `@release_post_manager`

**Maintain (rebase) the branch**

Occasionally rebase this MR by typing in the command `/rebase` in a new comment in this MR. This will help keep your branch up to date with `master`. Consult with the Tech Advisor as needed.

Occasionally remove the `Draft:` pre-fix from the subject of this MR and run the pipeline by clicking **Run Pipeline** in this MR's Pipelines tab. When the pipeline is complete, add the `Draft:` pre-fix back to the MR title to prevent accidental merge. Consult with the Tech Advisor as needed.


**General Content Review**

As PMs finalize their release post items it can be helpful for the RPM to review and offer feedback. This reduces pressure on the 17th as items are merged and provides additional review from someone with a fresh perspective. You can start this as early as the 12th, but this should be an ongoing task leading up to content assembly on the 18th. Review each MR labeled ~Ready for content that follows handbook guidance. See [What RPM should look for when reviewing content blocks](https://about.gitlab.com/handbook/marketing/blog/release-posts/#what-rpm-should-look-for-when-reviewing-content-blocks).

To easily manage and track reviewed items do the following:

- [ ] To track release post items you haven't reviewed, bookmark filtered MR lists similar to the following:
  - [Release post item MRs with the ~Ready label](https://gitlab.com/dashboard/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=release%20post%20item&not[label_name][]=rp%20manager%20reviewed&label_name[]=release%20post&label_name[]=Ready)
  - [Release post item MRs for this milestone](https://gitlab.com/dashboard/merge_requests?scope=all&state=opened&label_name[]=release%20post%20item&not[label_name][]=rp%20manager%20reviewed&label_name[]=release%20post&milestone_title=14.3) (update to correct milestone)
  - [Deprecation item MRs (in the `gitlab` project) for this milestone](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?scope=all&state=opened&label_name[]=release%20post%20item&label_name[]=release%20post%20item%3A%3Adeprecation&milestone_title=14.3) (update to correct milestone)
- [ ] Add the ~"rp manager reviewed" label to any RP item you've reviewed.


**Reminding and Alerting DRIs**

It's important to keep DRIs up to date regularly with items they need to deliver for the release post. Early reminders are especially helpful due to how async and distributed GitLab team members are.

- [ ] Alert DRIs (PMs, EMs and others as needed) at least one working day before each due date (post a comment to the `#release-post` Slack channel)


### General contributions `@release_post_manager`

The release post is authored following a changelog-style system.
Each item should be in an individual YAML file.

#### Contribution instructions

See the Handbook: [Contributing to the release post](https://about.gitlab.com/handbook/marketing/blog/release-posts/#general-contributions).

#### Content blocks

**Due date: YYYY-MM-DD** (10th)

Product Managers are responsible for [raising MRs for their content blocks](https://about.gitlab.com/handbook/marketing/blog/release-posts/#pm-contributors) and ensuring they are reviewed by necessary contributors by the due date. Content blocks should also be added for any epics or notable community contributions that were delivered.

Product Managers are also responsible for making sure all [Tech Writing reviews (required), and PM Director and PMM reviews (recommended)](https://about.gitlab.com/handbook/marketing/blog/release-posts/#reviews) are done for their content blocks. To help reviewers prioritize what to review, PMs should communicate which content blocks are most important for review by applying the proper labels to the release post item MR prior to assigning the MR to reviewers. (ex: Tech Writing, Direction, Deliverable, etc). PMs can also [follow these guidelines](https://about.gitlab.com/handbook/marketing/blog/release-posts/#recommendations-for-optional-director-and-pmm-reviews) to help decide which content blocks should get PM Director and PMM reviews.

To enable Engineering Managers [to merge the content blocks](https://about.gitlab.com/handbook/marketing/blog/release-posts/#merging-content-block-mrs) as soon as an issue has closed, PMs should ensure all scheduled items have MRs created for them and have the Ready label applied when content contribution and reviews are completed.

Product Managers should only check their box below when **all** their content blocks (features, deprecations, etc.) are complete (documentation links, images, etc.). Please don't check the box if there are still things pending.

_Reminder: be sure to reference your Direction items and Release features._ All items which appear
in our [Upcoming Releases page](https://about.gitlab.com/upcoming-releases/) should be included in the relevant release post.
For more guidance about what to include in the release post, check the [Product Handbook](https://about.gitlab.com/handbook/product/product-processes/#communication#release-posts).

- [ ] TODO on the 10th: Copy the [product team checkbox template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/team-member-checkboxes.md) and paste it into the description here:

PASTE TEAM CHECKBOX LIST HERE

#### Recurring content blocks

**Due date: YYYY-MM-DD** (10th)

The following sections are always present and managed by the PM or Eng lead
owning the related area.

- [ ] Add GitLab Runner improvements: `@DarrenEastman`
- [ ] Add Omnibus improvements: `@dorrino`
- [ ] Add Mattermost update to the Omnibus improvements section: `@dorrino`

**Due date: YYYY-MM-DD** (15th)

- [ ] [Add Performance Improvements](https://about.gitlab.com/handbook/marketing/blog/release-posts/#create-mrs-for-usability-improvements-bugs-and-performance-improvements) section: `@release_post_manager`
- [ ] [Add Bug Fixes](https://about.gitlab.com/handbook/marketing/blog/release-posts/#create-mrs-for-usability-improvements-bugs-and-performance-improvements) section: `@release_post_manager`
- [ ] [Add Usability ](https://about.gitlab.com/handbook/marketing/blog/release-posts/#create-mrs-for-usability-improvements-bugs-and-performance-improvements) section: `@release_post_manager`

#### Final Merge

**Due date: YYYY-MM-DD** (17th)

Engineering managers listed in the MRs are responsible for merging as soon as the implementing issue(s) are officially part of the release. All release post items must be merged on or before the 17th of the month. Earlier merges are preferred whenever possible. If a feature is not ready and won't be included in the release, the EM should push the release post item to the next milestone.

To assist managers in determining whether a release contains a feature. The following procedure [documented here](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/34519) is encouraged to be followed. In the coming releases, Product Management and Development will prioritize automating this process both so it's less error-prone and to make the notes more accurate to release cut.

---

### Content assembly and initial review (`@release_post_manager`)

Note: All the `Final Content Assembly` and `Structural Check` steps happen in sequence on the 18th starting ~8am PST (`America/Los_Angeles`). If the Release Post Manager and Technical Writer are in significantly different timezones, coordinate ahead of the 18th to understand how this could impact working hours for each team member. If need be, the time of initiating the `final content assembly` and the subsequent coordinated tasks can be shifted, as long as `Final content review` with the CEO and CProdO begins no later ~12pm PST on the 19th, to allow enough time for feedback and updates.

 **Due date: YYYY-MM-DD** (12th)


- [ ] Per the instructions in the handbook, request [MVP nominations](https://about.gitlab.com/handbook/marketing/blog/release-posts/#mvp) with a link to an issue for collaboration. **Be sure to follow the instructions in the handbook page to maximize contributions to the MVP issue.**
- [ ] Remind PMs/EMs to contribute to the bugs, usability, and performance improvement MRs by commenting on the various Slack threads you initiated in #release-post, #development and #eng-managers by the 10th

 **Due date: YYYY-MM-DD** (15th)

- [ ] Select a [cover image](https://about.gitlab.com/handbook/marketing/blog/release-posts#cover-image) for the release post
- [ ] Verify that the selected cover image has not been used before.
  - Tip: MacOS users, navigate to the `source/images/` directory and use the search bar in the Finder to search for `cover`. Make sure the scope is set to only search **images**. This won't reveal all previous images, but the last couple of years have had pretty consistent naming.
- [ ] On the `release-X-Y` branch, add the cover image to `source/images/X_Y/X_Y-cover-image.jpg`. Tip: Be sure to use an underscore (`_`) between release numbers, not a hyphen or dash (`-`).
  - Note that as you add references to the cover image throughout the release post branch, (including for social sharing) you need to match the filename and extension **exactly**. For example, if your image in the directory is `cover_image.jpg` but in another file, you reference it as `cover_image.jpeg`, it will not display as the extension does not match, so the file will not be found.
- [ ] On the `release-X-Y` branch, in `sites/uncategorized/source/releases/posts/YYYY-MM-DD-gitlab-X-Y-released.html.md`, [add details about the source image](https://about.gitlab.com/handbook/marketing/blog/release-posts/#cover-image-license).
- [ ] Choose an [MVP](https://about.gitlab.com/handbook/marketing/blog/release-posts/#mvp) for this release based on what's surfaced in the MVP issue
  - [ ] If no MVP nominations have been added to the MVP issue by the 15th, send reminders in Slack with the link to the MVP issue. An easy way to do this is to respond to your original Slack solicitation posts and resend to the whole channel.
  - [ ] After one or more quality nominations have been received, choose one and notify the `#release-post` channel of your choice. Use this chance to solicit any last-minute nominations, and confirm that the contribution your MVP choice was nominated for will make it into this release.
- [ ] Before handing off the bugs, usability, and performance improvements MRs to the TW lead for final review, remind PMs/EMs about the content due date by revisiting and commenting on the Slack threads you created by the 10th in `#release-post`, `#development`, and `#eng-managers`. Let them know it's the "last call" and no further contributions to the MRs will be taken after the 15th.
- [ ] In the threads of this MR, look for a GitLab Mattermost update, similar to [this example comment](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/78893#note_546017926). Thank the poster, then tag the [Distribution PM](https://about.gitlab.com/handbook/product/categories/#distribution-group) as an FYI and ask them to [check the box](#recurring-content-blocks) and resolve the thread.
- [ ] If there are no deprecation entries in the RP, ask PMs in the `#release-post` channel if there are any new deprecations that have not been merged yet.
- [ ] Remind PMs to review existing deprecations on the [Docs page](https://docs.gitlab.com/ee/update/deprecations.html) in case any edits are needed.



 **Due date: YYYY-MM-DD** (17th)


- [ ] Mention the [Distribution PM](https://about.gitlab.com/handbook/product/categories/#distribution-group) in the `#release-post` channel, reminding them to add any relevant [upgrade warnings](https://about.gitlab.com/handbook/marketing/blog/release-posts/#important-notes-on-upgrading) by doing an [upgrade MR](https://about.gitlab.com/handbook/marketing/blog/release-posts/#upgrades).
- [ ] Finalize your [MVP](https://about.gitlab.com/handbook/marketing/blog/release-posts/#mvp) selection and work with the person that nominated the MVP to write the MVP section in `data/release_posts/X_Y/mvp.yml` on the `release-X-Y` branch.
  - [ ] On the `release-X-Y` branch, add the MVP's name and other profile info to `data/mvps.yml`.
- [ ] In the `#release-post` channel remind all PMs that it's the 17th, so they need to either have their EMs merge their release post item MRs or bump the milestone if they know it won't make it.

 **Due date: YYYY-MM-DD** (18th at 8 AM PT and NO earlier)


- [ ] Perform **final content assembly** by pulling all content block MRs merged to `master` into the release post branch by using the following commands locally (one command at a time):

  ```
  git checkout master
  git pull
  git checkout release-x-y
  git pull
  git merge master
  bin/release-post-assemble
  git push origin release-x-y
  ```
- [ ] Do a visual check of the `release-X-Y` content block and image folders to make sure paths and names are correct
- [ ] Make sure the **View App** button shows up in the MR as expected
- [ ] Do a visual check of the blog post and ordering of content blocks for secondary items to confirm they are grouped by stage in descending alphabetical order.
- [ ] Update the release post intro in the `sites/uncategorized/source/releases/posts/YYYY-MM-DD-gitlab-X-Y-released.html.md` with 4 primary features to highlight. To do this:
  - [ ] Make sure to remove the backticks around the features.
  - [ ] Mention the VP of Product Management `@adawar` in the appropriate section of this MR and ask them to add a suggestion to update the intro, choosing no more than 4 features to highlight in order of importance. The top feature will appear as the first feature in the release notes, and the top 2 features will be used in the title of the post.
    - Be sure to provide the VP of Product Management with a clear time/date to do this based on when you want to do the final reviews on the 19th.
    - Then post a follow-up to the VP Product Management in the `#release-post` channel with a link to the comment you've created in this MR, letting them know they need to take action by the date/time you designated or you'll choose the features to highlight and move forward.
  - [ ] Link the release post items mentioned in the intro to the item blocks within the release post. For example, for a feature named `"Define test cases in GitLab"`, the link from the introduction should point to `#define-test-cases-in-gitlab`.
  - [ ] Count the feature blocks to get the total number of improvements and add it to the intro, replace the `XX` in `from the XX improvements` including replacing (removing) the backticks in the `sites/uncategorized/source/releases/posts/YYYY-MM-DD-gitlab-X-Y-released.html.md`. This count includes the top feature, primary features, secondary features, usability improvements, and performance improvements. Do not count bugs, upgrades, etc. **You should use an approximate count (i.e. 40+ instead of 43)** because the number can shift after the release post goes live.
  - [ ] Mark the first primary item, based on what the VP of Product Management decided, to appear on the page as `top` while the rest can remain [`primary` or `secondary`](https://about.gitlab.com/handbook/marketing/blog/release-posts/#primary-vs-secondary).
- [ ] On the `release-x-y` branch, update the release blurb for the homepage in the file: `/sites/uncategorized/source/includes/home/ten-oh-announcement.html.haml` changing the following lines:
  - [ ] Update the `%h1 X.X` line to the latest version.
  - [ ] Update the text under the release description with a copy/paste of the blog title. For example, `GitLab X.X released with Feature A and Feature B`.
  - [ ] Update the `link_to` line with the URL of the blog. For example, `/releases/2021/06/22/gitlab-14-0-released/`.
  - [ ] Update the ` "name":"GitLab X.X"` line to the latest version.
  - [ ] Update the `releaseNotes` to the URL of the blog. For example, `"releaseNotes":"https://about.gitlab.com/releases/2021/06/22/gitlab-14-0-released/"`.
  - [ ] Update the `"softwareVersion":"X.X"` line to the latest version.

- [ ] Ensure that the social media sharing text for the click to tweet button on the bottom of the release post is available in the introduction.

- [ ] Notify the PM team in the `#release-post` channel that final content assembly has happened and all work must now shift from `master` to the release post branch via coordination with the release post manager.
  - Include a link to the review app (**View App**) page, asking them to make sure all their content is showing up as expected with correct image/video links, etc. As confirmation of their final review, the PMs should check off their [content and recurring blocks](#content-blocks) items in the release post MR.
  - Be sure to link them to the [content and recurring blocks](#content-blocks) section in the MR as part of the post.

**Note**: If the release post assembly script fails, look at the bottom of [this section](https://about.gitlab.com/handbook/marketing/blog/release-posts/#merge-individual-items-in-to-your-branch) of the release post handbook page for further instruction

 **Due date: YYYY-MM-DD** (18th)


- [ ] Label this MR: ~"blog post" ~release ~"review-in-progress"
- [ ] Make sure there are no broken links in the review app (**View App**) page (use a dead link checker like [Check my Links](https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf))
  - [ ] Links to confidential issues may be missed. It is helpful to check for broken links as an unauthenticated GitLab user (either logged out, in another browser, or in Incognito mode).
  - [ ] If there are links to external blogs that are still broken in the review app, check with PMs and others as needed to make sure the referenced blogs go live before the 22nd.
- [ ] Check all comments in the MR thread (make sure no contribution was left behind).
- [ ] Make sure all discussions in the thread are resolved.
- [ ] Assign the MR to the next reviewer (Technical Writer lead).

 **Due date: YYYY-MM-DD** (20th)


- [ ] Check if the number of features you added in the introductory paragraph has changed. To get the number, do a hand count of just the features (top, primary, secondary) in `/data/release_posts/X_Y`, and also count the number of items in the performance improvements and the usability improvements files in the current `release-X-Y ` branch. Do not include bugs, upgrades, etc. You can use an approximate count (i.e. 40+ instead of 43). Remove the backticks around the number if you have not already.
- [ ] Post in the #release-post channel:

  ```
  _Hello PMs! The following features are top/primary!_

  (Provide link to review app (**View App**) and tag the PMs for the top/primary features listed in the release post).

  _Please let us know if any of your merged primary release post items shifted out of the release after the 18th and will not make it into the final release packages by the 22nd._
  ```

  - Tell them they can check [this query](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&milestone_title=13.10&not%5Blabel_name%5D%5B%5D=released%3A%3Acandidate) (updated with the correct milestone) and check with their EMs to verify the features did make it into the release.
  - It is the Release Post Manager's responsibility to make sure any top/primary items mentioned in the introduction are accurate prior to the 22nd, because release post items can sometimes move in/out of the packaged release after the 18th, and this could affect the themes, headline, etc.
- If you learn that any top/primary items have moved in/out of the packaged release after the 18th, communicate this directly to stop or start associated actions, with the DRIs for:
  - [Technical Marketing](https://about.gitlab.com/handbook/marketing/strategic-marketing/technical-marketing/) (the TMM team), who produce demo videos per release.
  - [Social Marketing](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/), who produce feature campaigns per release.
  - [Corporate Communications](https://about.gitlab.com/handbook/marketing/corporate-marketing/corporate-communications/), who lead media outreach and may have produced a press release
  - Any other related efforts you're aware of, for example related blog posts.

---

### Other reviews

Ideally, the team should complete all the reviews by the 19th of the month, so that the 2 days before the release can be left for fixes and small improvements.

#### [Structural check](https://about.gitlab.com/handbook/marketing/blog/release-posts/#structural-check) (`@tw_lead`)

 **Due date: YYYY-MM-DD** (18th)


The structural check is performed by the technical writing lead: `@tw_lead`

Before starting the structural check:

- [ ] Make sure the deprecations in the current release post page also show up in the [deprecations doc](https://docs.gitlab.com/ee/update/deprecations.html). If there are any issues, you might need to:
  - Create a branch in the `gitlab` project.
  - Run `bin/rake gitlab:docs:compile_deprecations` to bring the doc up to date.
  - Ask in `#tw-team` for another TW to quickly review and merge.
- [ ] Ensure that the [docs release process](https://gitlab.com/gitlab-org/gitlab-docs/-/blob/main/doc/releases.md) **is started** (but not completed). This process should start around the same time as the release post structural check, and be completed soon after the release post goes live.

When starting the structural check:

- [ ] Add the label ~review-structure.

For suggestions that you are confident don't need to be reviewed, change them locally
and push a commit directly to save the PMs from unneeded reviews. For example:

- Clear typos, like `this is a typpo`
- Minor front matter issues, like single quotes instead of double quotes, or vice versa
- Extra whitespace

For any changes to the content itself, make suggestions directly on the release post
diff, and be sure to ping the reporter for that block in the suggestion comment, so
that they can find it easily.

In the `www-gitlab-com` repository:

- [ ] Check [frontmatter](https://about.gitlab.com/handbook/marketing/blog/release-posts/#frontmatter) entries and syntax.
- [ ] Check that the item's `name` contains backticks when referring to code. (Previously we had to use single quotes, but backticks work now.)
- [ ] Check all images (png, jpg, and gifs) are smaller than 150 KB each.
- [ ] Remove any `.gitkeep` files accidentally included.
- [ ] Add or check `cover_img:` license block (at the end of the post). Should include `image_url:`, `license:`, `license_url:`.
- [ ] Search for `available_in: [free, premium, ultimate]` and change to `available_in: [core, premium, ultimate]`.

In the review app:

- [ ] Check that images match the context in which they are used, and are clear.
- [ ] Check for duplicate entries.
- [ ] Search for the text `gitlab-ci.yml` and ensure there is a period before the filename, for example, `.gitlab-ci.yml`.
- [ ] Check that features introduced in this release do not mistakenly reference previous releases (this often happens after features slip to a future release after an RPI is already written). If, for example, the current release is 13.8, and an item reads: _"In GitLab 13.7 we introduced XXX..."_, this means the feature most likely slipped to 13.8. In that case, correct the text to _"In GitLab 13.8 we introduced XXX..."_. A search for two or three previous release numbers ("13.7", "13.6", and "13.5" in our example) in the review app should be enough to spot this.
- [ ] Check all dates mentioned in entries, ensuring they refer to the correct year.
- [ ] Check the anchor links in the intro. All links in the release post markdown file should point to something in the release post YAML file.
- [ ] Check for structural consistency between release post items. Check for consistent use of white space, lists, and punctuation.
- [ ] Run a dead link checker like [Check my Links](https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf) and ping reporters directly on Slack asking them to fix broken links.
  - [ ] Links to confidential issues may be missed. It is helpful to check for broken links as an unauthenticated GitLab user (either logged out, in another browser, or in Incognito mode).
- [ ] Run a spelling check against the release post's review app (**View app**) page. For example, you can use [Webpage Spell-Check](https://chrome.google.com/webstore/detail/webpage-spell-check/mgdhaoimpabdhmacaclbbjddhngchjik?hl=en) for Google Chrome.

In general:

- [ ] Report any problems from the structural check in the `#release-post` channel by pinging the reporters directly for each problem. Do NOT ping `@all` or `@channel`, and do NOT leave a general message that no one will pay attention to. If possible, ensure open discussion threads in this merge request track any issues.
- [ ] Notify the release post manager that you're done with the structural check needed for **final content review**, by pinging them in the `#X-Y-release-post-prep` channel.
- [ ] When the executive reviews are complete (on or after the 20th), post a comment in the `#whats-happening-at-gitlab` channel linking to the review app (**View App**) and merge request reminding the team to take a look at the release post and to report problems in `#release-post`. CC the release post manager and product operations DRI. Template to use (replace links):

  ```
  Hey all! This month's release post is almost ready! Take a look at the preview and either
  report any problems in #release-post, or leave a comment to the release post MR.
  MR: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/1234
  Review app (this link is _temporary_ and should only be shared internally):
  https://release-X-Y.about.gitlab-review.app/releases/YYYY/MM/DD/gitlab-X-Y-released/index.html
  ```
- [ ] Remove the label ~review-structure.
- [ ] Within 1 week, update the release post templates and release post handbook with anything that comes up during the process.

---

#### Final content review (`@release_post_manager`)

 **Due date: YYYY-MM-DD** (18th - 19th)

- [ ] Check to be sure there are no broken links in the View app (use a dead link checker like [Check my Links](https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf).
- [ ] Mention `@sytse`, `@sfwgitlab`, and `@adawar` in the `#release-post` channel when the post has been generated for their review per these [communication guidelines](https://about.gitlab.com/handbook/marketing/blog/release-posts/#communication).
- [ ] Capture any feedback from Slack into a single comment on the Release Post MR with action items assigned to the DRIs to address. Check the [content review guidelines](https://about.gitlab.com/handbook/marketing/blog/release-posts/#content-reviews) for more details.

#### Incorporating Feedback
 Due date: YYYY-MM-DD** (by the 20th)


- [ ] Make sure all feedback from CEO and Product team reviews have been addressed by working with the DRIs of those areas as needed.
- [ ] If you receive feedback about the ordering of the primary Items, you might need to adjust the order.
- [ ] If needed. re-order secondary items by adjusting the `titles` in the content blocks. More information to consider about altering secondary items in the [content review guidelines](https://about.gitlab.com/handbook/marketing/blog/release-posts/#content-reviews) and [feature order technical instructions](https://about.gitlab.com/handbook/marketing/blog/release-posts/#feature-order)
- [ ] Make sure there are no open feedback items in this MR or in the `#release-post` channel.
- [ ] On the 20th, ping Product Operations (`@fseifoddini`) for final check in the `#release-post` channel.
- [ ] After the Product Operations review, remove the ~review-in-progress label.
- [ ] By 10 AM PDT on the 20th, request that the VP of Product Management `@adawar` identify the 3-7 items for **What's New** by posting in the `#release-post` channel and link to [Creating an MR for What's New entries](https://about.gitlab.com/handbook/marketing/blog/release-posts/index.html#creating-an-mr-for-whats-new-entries) in the handbook. cc What's New DRIs Product Operations `@fseifoddini` and Growth `@mkarampalas` in the Slack post.
  - Inform the VPoPM that they need to:
    1. Create and finalize the MR draft by 11:59 PDT on the 21st.
    1. Merge the MR after the release post is live on the 22nd and images have been checked to load correctly locally.

---

### Preparing to merge to `master` (`@release_post_manager`)

#### On the 21st


- [ ] Mention `@community-team` in the `#swag` channel to ask them to send the swag pack to the MVP.
- [ ] Check if all the anchor links in the intro are working.
- [ ] Confirm there are no broken links in the review app (**View app**) with a dead link checker like [Check my Links](https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf)).
- [ ] Check the total feature count statement in the introductory paragraph to make sure the number stated is accurate, and if not, update it. To get the number, do a hand count of the top feature, primary features, secondary features, and performance improvements (do not count bugs, upgrades, etc.) in `/data/release_posts/X_Y` on the current `release-X-Y ` branch.
- [ ] Work with your [Technical Advisor](https://about.gitlab.com/handbook/marketing/blog/release-posts/#technical-advisors) to merge `master` into your release post branch before the 22nd. This reduces the risk of needing to resolve conflicts when merging the release post branch into `master` on the 22nd. This issue shows an example of how this process can be handled: https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10886. After discussing with your Technical Advisor you might wish to remove the `Draft:` prefix from the MR, or merge `master` into your branch multiple times before the 22nd.
- [ ] Check to make sure all unresolved threads on this MR are resolved and there are no merge conflicts. If you need help resolving merge conflicts or other technical problems, ask for help from the [Technical Advisor](https://about.gitlab.com/handbook/marketing/blog/release-posts/#technical-advisors) in the `#dev-escalation `channel in Slack then cross-post to the `#release-post` channel to make others aware.
- [ ] In the `#releases` channel, reach out to the [release managers](https://about.gitlab.com/community/release-managers/) to let them know you are running the release post. Ask them to keep you in the loop if there are any changes to the regularly anticipated timeline

#### On the 22nd (`@release_post_manager`)

#### At 12:30 UTC

- [ ] Read the [important notes](#important-notes) below
- [ ] Say hello in the `#releases` channel to let the release managers know you're online and awaiting their signal in `#release-post` to start the release post's merge process.
  - Release Managers will alert you in `#release-post` if there are any issues with the release. You can follow along on the release issue to see the packaging progress on the 22nd. Check the [issue list](https://gitlab.com/gitlab-org/release/tasks/-/issues/) to find the issue ([example issue](https://gitlab.com/gitlab-org/release/tasks/-/issues/1261)). The `#releases` channel is also a good place to track any updates or announcements.
  - If everything is okay, the packages should be published at [13:30 UTC](https://gitlab.com/gitlab-org/release-tools/-/blob/fac347e5fc4e1f31cffb018d90061ef4f25747f3/templates/monthly.md.erb#L104-125), and available publicly around 14:10 UTC.
- [ ] Check to make sure there aren't any alerts on in the `#release-post` and `#whats-happening-at-gitlab` channels.
- [ ] Check to make sure there aren't any alerts on this MR or merge conflicts.


### Merging to `master` (`@release_post_manager`)
#### At 13:50 UTC


After the release manager confirmed that the packages are publicly available by pinging you in Slack:

- [ ] Announce to the team in the `#release-post` channel that you are starting the final merge process, you will reach out for help if the MR fails, and that you will lead collaboration with the appropriate team members to resolve any problems.
  - Depending on the complexity of the failure it is recommended that you first try to resolve the issue yourself and then reach out to the `#dev-escalation` channel per [What to do if your pipeline fails or you have other technical problems](#What-to-do-if-your-pipeline-fails-or-you-have-other-technical-problems).
- [ ] Add the MR to the merge train at 14:10-14:20 UTC.
- [ ] Wait for the pipeline. This can take anywhere from 20-45 minutes to complete.
- [ ] Check the live URL on social media (after the MR is merged) with [Twitter Card Validator](https://cards-dev.twitter.com/validator) and [Facebook Debugger](https://developers.facebook.com/tools/debug/). Make sure that in both cases, you can see the cover image of the release post displayed along with social sharing copy
  - If the cover image is not displaying or you see other error messages, resolve these issues before merging this MR, consulting with the Tech Advisor, Product Operations, or Social team as needed.
  - You may get a warning from Facebook that says `Missing Properties - The following required properties are missing: fb:app_id` - this can be ignored.
- [ ] Check for broken links again after the post is live.
- [ ] Hand off social posts to the social team and confirm that it's ready to publish: Mention `@social` in the `#release-post` Slack channel. Be sure to include the live URL link and social media copy (you can copy/paste the final copy from the review app).
  - A member of the social team will schedule the posts at the next available best time on the same day. The social team will mark the Slack message with a ⏳ once scheduled and add scheduled times to the post thread for team awareness. Further details are listed below in the [Important Notes Section](#important-notes).
- [ ] Share the links to the post on the `#release-posts` and `#whats-happening-at-gitlab` channels in Slack.


#### What to do if your pipeline fails or you have other technical problems

For assistance related to failed pipelines or eleventh-hour issues merging the release post, reach out to release post [technical advisors](https://about.gitlab.com/handbook/marketing/blog/release-posts/#technical-advisors) for assistance in the `#dev-escalation` Slack channel. Cross-post the thread from `#dev-escalation` in `#release-post` so all Product Managers and release post stakeholders are aware of status and delays.

#### Important notes

- The post is to be live on the **22nd** at **15:00 UTC**. It should be merged as soon as GitLab.com is up and running on the new release version (or the latest RC that has the same features as the release), and after all packages are publicly available. Not before. Ideally, merge it around 14:20 UTC as the pipeline takes about 40 min to run.
- The usual release time is **15:00 UTC** but it varies according to the deployment. If something comes up and delays the release, the release post will be delayed with the release.
- Coordinate the timing with the [release managers](https://about.gitlab.com/community/release-managers/). Ask them to keep you in the loop. Ideally, the packages should be published around 13:30-13:40, so they will be publicly available around 14:10 and you'll be able to merge the post at 14:20ish.
- After the release post is live, wait a few minutes to see if anyone spots an error (usually posted in the `#whats-happening-at-gitlab` or `#company-fyi` channels), then follow the `handoff to social team` checklist item above.
- The tweet to share on Slack will not be live, it will be scheduled during a peak awareness time on the 22nd. After the tweet is live, the social team will share the tweet link in the `#release-post` and `#whats-happening-at-gitlab` Slack channels.
- Keep an eye on Slack and in the blog post comments for a few hours to make sure no one found anything that needs fixing.


/label ~"blog post" ~release ~"release post" ~"priority::1"
/assign me
