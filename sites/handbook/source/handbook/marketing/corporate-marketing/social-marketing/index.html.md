---
layout: handbook-page-toc
title: Social Marketing Handbook
description: 'Strategies, Workflows, and Emojis for Social Media at GitLab'
twitter_image: /images/opengraph/handbook/social-marketing/social-handbook-top.png
twitter_image_alt: GitLab's Social Media Handbook branded image
twitter_site: gitlab
twitter_creator: gitlab
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to the Social Marketing Handbook

_The social marketing team is responsible for the stewardship of the GitLab brand social channels. We’re accountable for the organic editorial calendar and work with partners across the community relations, digital marketing, talent brand, and other teams to orchestrate the social media landscape for GitLab._

- [Learn what we report on and how we collate our data on the Social Media Reporting page](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/social-reporting/)
- [Head to our Social Media Administration Handbook page](/handbook/marketing/corporate-marketing/social-marketing/admin/) for administrative details and project management
- [Learn more about the social, community operations, and expert shared Community Management practices](/handbook/marketing/corporate-marketing/social-marketing/community-management/)
- [Get to know our company-wide social media policy and guidelines](https://about.gitlab.com/handbook/marketing/team-member-social-media-policy/) for all team members.
- [Step up your personal social media game with a GitLab Social Media Training and Certification](https://about.gitlab.com/handbook/marketing/corporate-marketing/corporate-communications-resourses-trainings/#social-media-trainings)

## Video introduction to Social Media at GitLab

<figure class="video_container"><iframe src="https://www.youtube.com/embed/XZQ2Egrk7tI"></iframe></figure>

## Social Media at GitLab has OKRs

Organic social media has a very particular way of succeeding. It's critical for the social team to lead the purpose and the OKRs that are focused on in order to align with best practices. While organic social media at GitLab may, at times, take requests from different teams with different objectives, it's important to remember that:

- it's more akin to storytelling than to direct marketing
- because our content goes worldwide and untargeted, organic social is top-of-funnel activity driving impressions, engagements, and conversations with assistance in link clicks to move users further down the funnel for the next team to work with
- the social team aids in corporate marketing OKRs as well as has our own to keep moving the practice at GitLab forward
- our long term goal is to continually make progress towards each of the above points

## Social Channels

| Channels under the care of the social team |
| ------------------------------------------ |
| [GitLab Brand Twitter](https://twitter.com/gitlab) |
| [GitLab Brand LinkedIn](https://www.linkedin.com/company/gitlab-com/) |
| [GitLab Brand Facebook](https://www.facebook.com/gitlab) |
| [GitLab Brand Instagram](https://www.instagram.com/gitlab/) |
| [GitLab Brand GIPHY Channel](https://giphy.com/gitlab) |

GitLab Branded Social Channels include our company [Twitter](https://twitter.com/gitlab), [LinkedIn](https://www.linkedin.com/company/gitlab-com), and [Facebook](https://www.facebook.com/gitlab) which are managed in a more traditional manner. And an [Instagram](https://www.instagram.com/gitlab) channel that takes more of an editorial look at remote life and work at GitLab. This channel is a little less “marketing-y” and more about sharing thoughtful stories by our team members. Our [GIPHY](https://giphy.com/gitlab) account is mainly for new GIF content, which can be tied to larger initiatives. And lastly, our [YouTube](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg) channel is a testament to the core of GitLab where everyone can contribute.

## Social-First content and campaigns

The social media team will often create our own content and campaigns for the sake of having fun and meeting our own objectives. This is also a critical part of a successful content strategy. If our social-first content is to align with a larger topic or talking point from GitLab, we will be sure to run our messaging by the DRI on the correct team.

## Private Strategies

While we're public by default, there will always be strategies and details that will need to be confidential to the teams working together. These grey area details are often moved to confidential status on a subjective basis and for the security and protection of the greater community, partners or other 3rd-parties, and for disclosure reasons. For these reasons, the links below are to confidential issues or epics, which we'll use to retain information and act as confidential handbook pages when needed.

#### [Organic Social and Partner Marketing](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/3154)

## Calendaring and content volume

We know that less is more, usually. However GitLab is a global all remote brand. We have users in every timezone and we are often organizing events and news to coordinate in ways that include people across the globe. Because of this, our calendaring and volume must remain adaptable. 

Content calendars are often at "full capacity" or what we'd consider: 
- 1-2x on LinkedIn per day
- 1-2x on Facebook per day
- 2-4x on Twitter per day

This is due to the sheer volume of content that we'd deem as valuable to our audiences, the need to continue driving traffic to our website, and to continuously include followers across timezones on our social channels. However, we will ultimately publish content at the best times of day, determined by Sprout machine learning, which may or may not include a wide variety of times. 

Our "full capacity" concept is not a requirement. We can and will evolve our calendaring volume and focuses as brand, community, and business needs change. If this does change, we'll need to establish proper expectations. 

## Channel Use

### Twitter <a name="twitter"></a>

Because the wider developer community utilizes [Twitter](https://twitter.com/gitlab) as a platform to celebrate contributions, make feature suggestions, and share industry ideas, GitLab prioritizes this channel. We post to this platform first, and also more frequently. 

On Twitter our GitLab community is our first priority. It includes those who contribute to our DevOps platform, our GitLab Heroes, and folks who evangelize the industry. We take time to listen to their feedback, even when it is critical. We make space for fun. Through inside jokes, polls, or simple questions, our Twitter channel provides developers an opportunity to take a break and relate to one another. GitLab’s Twitter community is unique because we focus on engaging with their opinions. And delivering them tailored content they will actually find useful, or inspiring. 

The GitLab Brand Twitter channel is establishing a sense of community through these activities: 
 
<details>
 <summary markdown='span'>
   Posting frequently
 </summary>
 
Twitter does not operate like our other GitLab Social Branded Channels. There are no strict frequency limitations on Twitter and we aim to post at a minimum of  3 times a day if not more. Retweets and quote tweets scheduled to the calendar do not reduce our limitation. We see 1:1 interactions as useful, limitless, and should always be added to the calendar if they correlate with a promotion, or simply celebrate a milestone. Every post should take advantage of Sprout’s optimal time feature as we have many followers located across all time zones.
Do not always select the 3-5 stars optimal time. If there are already 4 posts going out that day select the 1-2 stars optimal time. This will help keep the calendar properly spaced out and ensure we have content publishing throughout the day.
</details>

<details>
 <summary markdown='span'>
   Community management
 </summary>
 
GitLab’s Twitter ecosystem of engagement includes mentions from code contributors, brand personalities, technical evangelists, GitLab Heroes, remote work enthusiasts,  and team members. We prioritize engaging with these audiences because it enables the brand to drive more meaningful interactions through social media. That means we use the Smart Inbox feature to find posts to like, comment, or retweet on a daily basis. We commit to quote tweeting posts when someone shares their #MyFirstMRmerged, a contribution to a project, or excitement about a new feature. 

GitLab also participates in periodic giveaways which are launched directly on our Twitter channel. The barrier to entry often includes a social share and tag. Our audience has responded really well to these easy-to-enter giveaways that provide them free passes or swag items. We believe in these 1:1 relationships that we’ve built online. It feels good to give back to our community who makes us a loveable product, and brand. 
</details>

<details>
 <summary markdown='span'>
   Story telling
 </summary>
 
We’re human. And enable humans to build better software, together. So to share our stories with the world, we don’t just quickly share a piece of content. We develop organic social media campaigns that have our audience and their reactions in mind. We take a corporate blog- and craft it into an editorial campaign. We display real voices through creative imagery. We share relatable experiences, and bring light to specific features that make devops- easier. We don’t share headlines- we develop a deeper understanding. 
</details>

<details>
 <summary markdown='span'>
   Polls & Questions
 </summary>
 
Creating space to interact with the GitLab community has always been a priority for our Twitter channel. By posting frequent polls, or questions- we found a way to increase engagement and also generate two way conversation. We ask a variety of questions, both silly and thought-provoking. 
</details>

<details>
 <summary markdown='span'>
   Emoji & GIF use
 </summary>
 
Our tone of voice on social media is human and friendly. All devs are friends; no matter the tool they use. That’s why we use emojis whenever we can. The overuse of emojis allows us to participate in a universally used language. GitLab believes that everyone can contribute. That’s why being better communicators is crucial for our audience- no message should go misunderstood.

When the opportunity presents itself, using GIFs in quote tweets, comments, or celebratory posts has proven to increase engagement. 
</details>

### LinkedIn <a name="linkedin"></a>

Our [LinkedIn](https://www.linkedin.com/company/gitlab-com/mycompany/) community consumes content over conversation, first. Though there are opportunities to drive conversation related to company culture, remote work, and exciting announcements. GitLab's LinkedIn page strategically keeps our followers reading, and spending time on our blogs, releases, topic pages, and use cases. Ultimately, we use Linkedin as an opportunity to expand in more detail on the content we care about. How do we do that? By using a variety of formatted posts with more thoughtful text, questions, polls, quotes, or even statistics. Due to the nature of the platform, there is less 1:1 conversation, and engagement with the community. We also have a decreased frequency of posts in comparison to our Twitter channel. How often GitLab should post on LinkedIn depends on the time of year, and the content we are sharing. Typically we post every single day, and up to2 times per day. However, we do make exceptions for special events, and major brand-level campaigns. 

The GitLab Brand LinkedIn channel is establishing GitLab as the complete DevOps platform through these activities: 
 
<details>
 <summary markdown='span'>
   Posting consciously
 </summary>
 
LinkedIn does not operate like Twitter. We publish two posts per day with the exception of timely announcements that must take place. Content shared should prioritize our culture, blog, releases, remote work, and press mentions. Because there is a limited post frequency, every post published should be during Sprout Social's 5 star and 4 star optimal times. This will ensure the few posts we share, are seen by followers located across all time zones.

By posting consciously, we don't just consider the time a post goes live. When the social media team is supporting brand-level campaigns or majors events like Commit or KubeCon. It is important to include content that supports the wider marketing organization's OKR's. That means if there is a Pathfactory link available, the social media manager should direct folks there first. 

</details>

<details>
 <summary markdown='span'>
   Evangelizing our values 
 </summary>
 
Though LinkedIn doesn't function like Twitter, you can strategically encourage opinions with thought provoking and also relatable content. For example, [in this post](https://www.linkedin.com/feed/update/urn:li:activity:6811300910724087808) we normalized the celebration of gray squares in your contribution chart. This started a very transparent and honest discussion about the hours people work, the days they contribute on, and also don't. When publishing content centered around our GitLab values, the social media manager always responds to comments thoughtfully and intentionally to create a stronger connection to our brand. 

</details>

<details>
 <summary markdown='span'>
   Responding as the company
 </summary>
 
It is important to amplify partner mentions, contributors and their wins, and GitLab team members' channels when the social media manager sees fit. So if there is an exciting partnership that announces a new integration, a hackathon winner, or even a "we belong here" story- these are all great examples that should be engaged with or shared directly to our company page on LinkedIn. [Please see this section of the handbook for instructions on how to do that.](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/#responding-as-the-company-on-linkedin-for-any-post) 
</details>

<details>
 <summary markdown='span'>
   Press mentions
 </summary>
 
Promoting GitLab as a thought leader, and loveable brand has always been a priority for Social @ GitLab. By sharing third-party content or press mentions, we increase engagement and impressions on LinkedIn. For example, [in this post](https://www.linkedin.com/posts/gitlab-com_gitlab-named-one-of-the-best-workplaces-2021-activity-6801254950870507520-74oI) GitLab is named one of the Best Workplaces 2021 and was a top post by engagement. The social media team should monitor the #external-comms channel to find more opportunities. 
</details>

### Facebook <a name="facebook"></a>

The reality is that GitLab has a smaller audience on [Facebook](https://www.facebook.com/gitlab/) so its priority does fall below our efforts on Twitter and LinkedIn. It is still important to maintain a presence on this channel because it provides folks basic information about our business, and proves to still see organic reach. That said, there is a decreased frequency for this channel. And social media team also chooses not to share certain types of campaigns to this channel as it is not the right audience. 

The GitLab Brand Facebook channel is reinforces our presence as a lovable brand through these activities: 
 
<details>
 <summary markdown='span'>
   Posting occasionally
 </summary>
 
We aim to post once a day on Facebook to maintain a presence and our social media landscape. though, there is no immediate need to post every single day on Facebook. If content is not available, then a post does not need to publish that day. During heavy calendaring times- there should be no more than 2 posts a day on Facebook. Organic Facebook posts can cannibalize ROI from other organic posts, so a high frequency of posts will inevitably have a lower reach per post. This tactic should only be used during critical times meaning for timely announcements, or news that requires more than one post that day. 

</details>

<details>
 <summary markdown='span'>
   Tailoring content 
 </summary>
 
Facebook content should prioritize the following: company culture, remote work, our community, values, features, and announcements. 

Campaigns that require you to sign up will not see success on this channel, and should only be published on Twitter and LinkedIn. 

</details>

### Instagram <a name="instagram"></a>

In no way shape or form should our [Instagram](https://www.instagram.com/gitlab)Instagram account mirror our other GitLab Branded Social Channels. In fact, it takes more of an editorial approach to it, and is less “marketing-y” and more about building community.

Since launching the GitLab Brand Instagram channel the Social Media Team committed to posting weekly content driven by a dedicated theme which represents our values, remote work, or life at GitLab. 

[FY22Q1: We Belong Here](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/4140)

[FY22H1: Cribs](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/4399)

Future themes TBD

The GitLab Brand Instagram channel reinforces us as an all-remote organization that really awesome humans want to work for through these activities: 
 
<details>
 <summary markdown='span'>
   Posting weekly
 </summary>
 
We aim to post once a week on Instagram to maintain a steady and ever increasing follower growth. Being consistent with your frequency will avoid seeing a drop in engagement and give folks a reason to follow. It also increases the chance of our content being discovered. 

Because we only post once a week on this channel, it should always be scheduled during the most optimal time according to Sprout Social. Note that if you are scheduling more than one photo per post, you will have to publish natively through the Sprout Social app. [Here are instructions on how to do that.](ttps://support.sproutsocial.com/hc/en-us/articles/360042617251-Carousel-Posts-on-Instagram)

</details>

<details>
 <summary markdown='span'>
   Instagram Stories
 </summary>
 
The stories feature on Instagram should be used to extend the life of the feed post you shared that week. After engagement on the post has slowed, use the story feature on Instagram. The story does not have to replicate your recent post. Instead it increases reach and visits to the Brand Instagram channel.  

</details>

<details>
 <summary markdown='span'>
   Storytelling
 </summary>
 
We want to share real-life stories and experiences with our Instagram community to create a stronger connection with them. We use the caption as an opprtunity to share a voice of a GitLab team member who is living our values. We extend the employer brand and incite fun conversation around remote working.   

</details>

<details>
 <summary markdown='span'>
   Respond to comments
 </summary>
 
The GitLab Instagram channel should really feel human. That's why you should respond to every single comment that isn't from an automated bot. Even a simple emoji, or like can go a long way. It reinforces the connection to our brand.  

</details>

<details>
 <summary markdown='span'>
   Hashtags and account tags
 </summary>
 
Each campaign/theme executed on the Instagram Brand Channel should include hashtag research, and specific hashtags to use at launch. By using strategic hashtags, and tagging specific accounts, your post will be seen beyond the feed. You will increase your chances of being discovered in Instagram's Explore page. The Instagram Explore page has an algorithum that improves content recommendations. [Here are instructions on how to do that.](https://sproutsocial.com/insights/instagram-explore/)

</details>

### Medium <a name="medium"></a>

GitLab has a [Medium publication](https://medium.com/gitlab-magazine), and all GitLab team-members may be added as writers! To be added as a writer to the publication, [import](https://help.medium.com/hc/en-us/articles/214550207-Import-post) a blog post that you authored on about.gitlab.com/blog to your personal Medium account, and submit it to the GitLab publication (by hitting `edit` -> `submit to publication` -> `GitLab Magazine`). the Social Marketing Manager will approve you as a writer and help finalize the post before publishing.

If you submit original content (i.e., not originally published somewhere else) to the publication for review, she may edit and publish your post. We want to highlight writers wherever possible, so we highly encourage you to import posts to your personal Medium.

**Content/Execution**

- Brand and thought leadership-focused
- Favorite articles about GitLab
- Aim to post one article to Medium every two weeks
- Cross-post our original thought-leadership posts to Medium, linking back to blog
    - Use the [import tool](https://help.medium.com/hc/en-us/articles/214550207-Import-post); do not copy and paste.

**Medium is not currently in the social or editorial workflow and is current depreciated. This entry is left in the handbook to maintain transparency.**

### YouTube <a name="youtube"></a>

**Content/Execution**

- Developer/community-focused
- Live events are more tech focused
    - Group Conversations, pick your brain meetings, demos, brainstorms, kickoffs

## Social Media Design

While we look to the design team to provide integrated campaign assets and elements to use on social, many rapid design needs are fulfilled from the social team directly by Using Canva Pro. Canva Pro provides the social team with an easy-to-use tool on any device to quickly spin up designs.
Social Media requires rapid responses and quick turnarounds, so to do we require the same of our designs.

While Canva does provide many pre templated layouts, we do not use those. The social team will build custom art using basic GitLab brand guidelines and best practices for social media. In the future, many templates and individual assets will be brand team approved, but we have not iterated far enough to work in a rapid process.

The elements the social team uses for our designs are mainly GitLab-owned. Icons, sketches, animations, etc. were usually created for non-social purposes and we've added them to our library to use for social media. We do use Pro-only elements on occasion, but strive to find elements we feel would not be used often by other brands.

### Social image sizes 

[While each channel has their own best sizes to use](https://sproutsocial.com/insights/social-media-image-sizes-guide/), and changes often, for the sake of scale and efficiency, we'll use the following dimensions unless otherwise noted. 

- 1800 x 945 for Twitter, Facebook, LinkedIn, and about.gitlab.com embedded social sharing cards (or opengraphs)
- 1620 x 1620 for Instagram wall posts (unless we are deliberately using IG's vertical or horizontal options)
- 1620 x 2880 for stories (vertical) posts on any channel that supports stories 

Here's the strategy behind the decisions above:
- Images in the dimensions of 1800 x 945 are currently visible correctly across Facebook, LinkedIn, and Twitter. In some instances, the images are a bit off-center on Twitter's mobile app, however, the majority of our audience engages with us via desktop, which puts our visual strategies into a desktop-first experience. They are also the correct size for opengraph cards. This may change in the future, as channels continue to tweak how their sites show images. 
- The specific pixel size of these images should be larger than what is advertised (e.g. every image standard above is 1.5x the recommended pixel size). This is because each channel has its own way of processing images that are uploaded to the site, and in many cases, a larger image results in a higher quality visual when either downsampled (reduced quality) or shrunk to fit (the larger size is seen smaller, making the image sharper). 

## Various Other Details

### Sharing 3rd-party events (onsite or virtual) on organic social where a team member is speaking

The purpose of GitLab team members participating in 3rd-party events is to bring GitLab's message to the 3rd-party's audience and to gain more community. Therefore, it's contradictory for GitLab brand social channels to promote a team member speaker for a 3rd-party event. Periodically, if the speaking engagement is part of a critical campaign or would resonate with the zietgiest of the moment, we may tweet once for folks to register for the event. However, if you're the speaker, we encourage you to post to your own social channels so that your network can join the event. Share your post with the #social_media Slack channel and we'll like, comment, and maybe share your post!

### Sharing blog posts

New blogs are linked in the #content-updates channel, and then our team  uses that as a notification that the blog is ready to schedule and share on social. Sometimes when it’s connected to a campaign or is particularly important, we’ll also get a ping about it in a thread from the original slack message.

If the blog is connected to an integrated campaign, it will be picked up in the social issues and epics for that campaign. If it is a part of a press release/announcement, we'll also be tagged in related issues through the PR workflow.

On a quarterly basis the social media team will identify the best performing blogs. The top 2-3 blogs will then be reintroduced and scheduled to publish on GitLab's Brand Social Channels.

Workflow: 
- Social team member sees new blogs in the #content-updates Slack channel
- Take the link and head to [Sprout's publishing settings to set the UTMs](https://app.sproutsocial.com/settings/publishing/)
- Under URL tracking, click "+add url"
- Set the parameter for `campaign` and save it. For basic blogs, the campaign is `blog`. If it can be (or is explicitly) tied to a marketing campaign, use that campaign utm. e.g., `10years`, `seeingisbelieving`, `devopsplatform`.
- [Head to Sprout's publishing tab](https://app.sproutsocial.com/publishing/), and schedule blog content. When you add the URL of the blog to the post, it will automatically add UTMs based on the channel and use the preconfigured campaign UTM from above.

### Sharing content from behind a paywall

We'll come across articles that are behind a pay wall from time to time, mainly when it's press coverage we're looking to share. We can still share these articles on our social channels, however, we'll need to disclose that there is a paywall in the copy of the social post, as to be transparent with our community. Clicks that come from folks who can't read the article aren't distinguishable from clicks where folks can read below the fold, so it's important to be clear about the differences.

##### Suggested paywall disclosure copy

Add the paywall disclosure copy to the very end of the copy. Consider using a line break to make this sentence explict to the community.

<details>
  <summary markdown='span'>
    LinkedIn, Facebook, other channel without character limitations
  </summary>
Because this article is behind a paywall, you may need to be a subscriber to read it in its entirety.
</details>

<details>
  <summary markdown='span'>
    Twitter or other channel with character limits
  </summary>
This article is behind a paywall.
</details>

### Responding as the company on LinkedIn for any post

Because of limited API access, it's challenging to engage and respond with posts on LinkedIn that do not directly @mention the company page. However, you can use the following method.

Every specific LinkedIn post from a page or a person has its own URL, [like this one](https://www.linkedin.com/posts/wilspillane_open-source-social-media-strategies-from-activity-6674298269985767424-4SEx/). At the end of the URL, there is a string of characters at the end, like `6674298269985767424-4SEx/` from the example above. If you add the following to the end of the post URL when you are an admin of the GitLab LinkedIn page, you'll be able to engage and comment as GitLab.

`?actorCompanyId=5101804`

The URL example from earlier is
`https://www.linkedin.com/posts/wilspillane_open-source-social-media-strategies-from-activity-6674298269985767424-4SEx/`.

Adding the `actorCompanyId` to the url would look like
`https://www.linkedin.com/posts/wilspillane_open-source-social-media-strategies-from-activity-6674298269985767424-4SEx/?actorCompanyId=5101804`

This URL would allow for GitLab to comment and engage.

## Social during an incident

### Going Dark

When an incident occurs, it may be appropriate for the social team to pause all brand channel social posts. This is called "going dark", and is a regular part of evaluating whether or not company messaging is appropriate to share at any given time. It is considered good practice to minimize the digital space brands occupy on social media during these times. This can help to not distract social conversation but can also reduce the probability of being accidentally caught up in conversation unintentionally, sounding tone-deaf, or otherwise coming across as insensitive.

When this occurs, time-sensitive posts will be the first to be rescheduled. In the event time-sensitive posts could not be rescheduled, the social team will do what we can to update our stakeholders on what posts won't be published. Non-time sensitive posts will be moved to Sprout drafts, which can then be rescheduled at a later time.

Going dark could have a negative impact on social metrics, depending on the severity and length of time we're dark. Going dark could also negatively impact specific CTAs if we're relying on organic social to perform. These considerations are a part of making the decision and will be communicated as often as appropriate.

### Rewriting copy or switching assets

It may be necessary to update copy or assets depending on cultural impact during an incident. The social team reserves the agency to do this as we see necessary, however, we will communicate the changes to our stakeholders if we'd consider the changes to be major.

## Support and status communications on social media

For GitLab status updates, folks can find info at the [@gitlabstatus Twitter handle](https://twitter.com/gitlabstatus). This account is not managed by the social team.

Beyond general inquries and regular social engagement with our followers, we do not provide core support in private messages on social channels. We will instead direct followers to the [GitLab forums](https://forum.gitlab.com/) for support.

Please use a derivative of this message:

```
Thanks for reaching out! In our spirit of transparency, we don't provide support here. Please add your question to the Q&A section of the GitLab forum, so that everyone can learn the answer. https://forum.gitlab.com/c/questions-and-answers/7
```

_The above template is also located in Sprout for quick access_
<!-- Social Marketing Handbook - ref https://gitlab.com/gitlab-com/marketing/issues/524 -->
<!-- identifiers, in alphabetical order -->

[blog]: /blog/
[description-tag]: http://www.wordstream.com/meta-tags
[facebook debugger]: https://developers.facebook.com/tools/debug/
[first post]: /2016/07/19/markdown-kramdown-tips-and-tricks/
[OG]: https://developers.facebook.com/docs/sharing/webmasters#markup
[second post]: /2016/07/20/gitlab-is-open-core-github-is-closed-source/
[twitter card validator]: https://cards-dev.twitter.com/validator
[twitter cards]: https://dev.twitter.com/cards/overview
[twitter-card-comp]: /images/handbook/marketing/twitter-card-complete.jpg
[twitter-card-incomp]: /images/handbook/marketing/twitter-card-incomplete.jpg
[www-gitlab-com]: https://gitlab.com/gitlab-com/www-gitlab-com
