---
layout: handbook-page-toc
title: "Sales & Customer Success Onboarding"
description: "Sales onboarding at GitLab is a blended learning experience focused on what new sales team members need to know, do, and be able to articulate within their first 30 days or so on the job"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## The Goals of Sales & Customer Success Onboarding
Sales onboarding at GitLab is a blended learning experience (virtual, self-paced learning path paired with an immersive, hands-on in-person workshop called Sales Quick Start) focused on what new sales team members need to **KNOW**, **DO**, and **BE ABLE TO ARTICULATE** within their first 30 days or so on the job so they are "customer conversation ready" and they are comfortable, competent, and confident leading customer engagements before the end of their first several weeks on the job. There are some formal learning components with a heavy emphasis on social learning (learning from others) and learning by doing to help bridge the knowing-going gap. 

For a detailed list of SQS Learning Objectives, please visit [this page](https://about.gitlab.com/handbook/sales/onboarding/sqs-learning-objectives/).

## Key SQS Details

*  [Target Roles](https://about.gitlab.com/handbook/sales/onboarding/target-roles/)
*  [Sales Onboarding Process](https://about.gitlab.com/handbook/sales/onboarding/sales-learning-path/onboarding-process/)
*  [Sales & Customer Success Quick Start Learning Path](https://about.gitlab.com/handbook/sales/onboarding/sales-learning-path/)
*  [SQS Workshop](https://about.gitlab.com/handbook/sales/onboarding/SQS-workshop/)
*  [Graduating from Sales Onboarding](https://about.gitlab.com/handbook/sales/onboarding/graduating-SQS/)

## Upcoming Sales Quick Start (SQS) Workshops

| DATES | GEO | LOCATION | ISSUE |
| ------ | ------ | ------ | ------ |
| Jul 6 - Jul 16 2021 | AMER / EMEA | Virtual | [SQS16 issue](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/issues/827) | 
| Aug 30 - Sep 9 2021 | AMER / EMEA | Virtual | [SQS17 issue](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/issues/896) | 
| Oct 4 - Oct 14 2021 | AMER / EMEA | Virtual | [SQS18 issue](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/issues/902) | 
| Dec 1 - Dec 10 2021 | AMER / EMEA | Virtual | [SQS19 issue](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/issues/953) | 



## Swag for New Sales Team Members

As a part of Sales Onboarding, each new Americas Sales team member is allowed to order one Swag Marketing Kit through this [form](https://docs.google.com/forms/d/e/1FAIpQLSflKtSu5xyYERATBHGswwMjn4NsUc8DMTxfKQXDAZ0FqdEYCg/viewform). As of right now, there is no Swag Marketing Kit available for our other regions, but the Marketing team is working towards developing one. 
