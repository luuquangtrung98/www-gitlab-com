---
layout: handbook-page-toc
title: The GitLab Procurement Team
---

{::options parse_block_html="true" /}

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc}

- TOC
{:toc}


## <i class="far fa-paper-plane" id="biz-tech-icons"></i> What is Procurement?
Procurement is the process of selecting vendors, strategic vetting, selection, negotiation of contracts, and the actual purchasing of goods. Procurement acquires all the goods, services, and work that is vital to GitLab.

## <i class="far fa-paper-plane" id="biz-tech-icons"></i> Procurement Mission and Policy
- The mission of the Procurement Team at GitLab is to be a strategic business partner for all departments to drive value through external partnerships.
- Team members can purchase goods and services on behalf of the company in accordance with the [Signature Authorization Matrix](/handbook/finance/authorization-matrix/) and guide to [Spending Company Money](/handbook/spending-company-money/). 
- All purchases made on behalf of GitLab that do not qualify as a personal expense, need an approved Purchase Order (PO) in Coupa. 

**Exceptions to the PO Policy are:**
1. Purchases under $5K that are NOT Software
      - ALL Software needs an approved PO in Coupa regardless of dollar value due to security risk requirements.  
1. Last minute un-planned onsite event needs such as food and rental transactions needed for event.
1. One-time field marketing and event purchases less than $10K such as booth rentals, AV equipment, catering, etc.
1. Confidential Legal Fees
1. Audit and Tax Fees
1. Benefits & Payroll

## <i class="fas fa-stream" id="biz-tech-icons"></i> What are you buying?

Choose the type of purchase you're making for the process guidance:
<div class="flex-row" markdown="0" style="height:110px;justify-content:center">
  <a href="/handbook/finance/procurement/new-software/" class="btn btn-purple" style="white-space: initial;min-width: 0;width: auto;margin:5px;display:grid;align-items:center;height:100%;">New Software</a>
  <a href="/handbook/finance/procurement/software-renewal/" class="btn btn-purple" style="white-space: initial;min-width: 0;width: auto;margin:5px;display:grid;align-items:center;height:100%;">Software Renewal or add-on</a>
  <a href="/handbook/finance/procurement/professional-services/" class="btn btn-purple" style="white-space: initial;min-width: 0;width: auto;margin:5px;display:grid;align-items:center;height:100%;">Professional Services / Contractors / Agencies</a>
  <a href="/handbook/finance/procurement/field-marketing-events" class="btn btn-purple" style="white-space: initial;min-width: 0;width: auto;margin:5px;display:grid;align-items:center;height:100%;">Field Marketing and Events</a>
  <a href="/handbook/finance/procurement/office-equipment-supplies/" class="btn btn-purple" style="white-space: initial;min-width: 0;width: auto;margin:5px;display:grid;align-items:center;height:100%;">Home office equipment and supplies</a>
</div> 

## <i class="fas fa-stream" id="biz-tech-icons"></i> Toolkits
Includes RFP Templates, scorecards and Vendor Guidelines for doing business with GitLab:
<div class="flex-row" markdown="0" style="height:110px;justify-content:center">
  <a href="/handbook/finance/procurement/vendor-selection-process/" class="btn btn-purple" style="white-space: initial;min-width: 0;width: auto;margin-left:30px;margin-right:30px;display:grid;align-items:center;height:100%;">Vendor selection process</a>  
  <a href="/handbook/finance/procurement/vendor-guidelines/" class="btn btn-purple" style="white-space: initial;min-width: 0;width: auto;margin:5px;display:grid;align-items:center;height:100%;">Vendor guidelines</a>
</div>

## <i class="fas fa-stream" id="biz-tech-icons"></i> How do I contact Procurement?
- If you have a question, use the #procurement slack channel.

## <i class="fas fa-stream" id="biz-tech-icons"></i> How do I access Coupa to create a Purchase Request?

Coupa is available via Okta. To access the platform:
1. Login to your [Okta home page](https://gitlab.okta.com/app/UserHome#).
1. Click on the Coupa (Prod) button.
   - A new tab should open with your user logged in.

Every month all Coupa access will be reviewed and users who haven't been active in a period of 90 days will have their access removed.
If you need to request access again, please reopen your initial Access Request issue and tag the Finance Systems Admins team using `@gitlab-com/business-technology/enterprise-apps/financeops` in a comment.
{: .alert .alert-warning}

If your job function requires you to submit purchase requests in Coupa, follow the below steps:
1. Open an [Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new) for Coupa using the `Individual_Bulk_Access_Request` template.
1. In Step 2, in the _Justification for this access_ question, please describe:
   - What you need to buy.
   - What is the total cost of the purchase. 
   - How often you will need to purchase it.
   - When does it need to be submitted in Coupa.
1. Add the labels ~"FinSys - Coupa" and ~"FinSys::Service Desk".

<div class="panel panel-success">
**Best Practices**
{: .panel-heading}
<div class="panel-body">
Due to the limited number of licenses available for Coupa, it is recommended that each department identify power users responsible for creating purchase requests on the team’s behalf.

</div>
</div>


## <i class="fas fa-book" id="biz-tech-icons"></i> Related Docs and Templates

##### Contract Templates

- [Mutual Non-Disclosure Agreement (NDA)](https://drive.google.com/file/d/1kQfvcnJ_G-ljZKmBnAFbphl-yFfF7W5U/view?usp=sharing)
- [Logo Authorization Template](https://drive.google.com/file/d/1Vtq3UHc8lMfIbVFJ3Mc-PZZjb6_CKAvm/view?usp=sharing)
- [Data Processing Agreement](https://drive.google.com/file/d/1zR-QYz8Hnk5XR-DaFNlzrEksL0hyFsjT/view?usp=sharing)
- [US-based Independent Contractor Agreement](https://drive.google.com/file/d/1NIon0c92ej2CRABQsdzWbpdM-3cJy0Se/view?usp=sharing) Master agreement for a subcontractor performing internal services directly to GitLab.
- [MPS Subcontractor Agreement](https://drive.google.com/file/d/1MgBqa_K1oqqIR79jV3AEynbYN6Jc_V5U/view?usp=sharing) Master agreement for subcontractor performing services for the **GitLab Customer**.

#### Documentation

* [Uploading Third Party Contracts to ContractWorks](/handbook/legal/vendor-contract-filing-process/)
* [Company Information](https://gitlab.com/gitlab-com/Finance-Division/finance/-/wikis/company-information) - general information about each legal entity of the company
* [Trademark](/handbook/marketing/corporate-marketing/brand-activation/brand-standards/#trademark) - information regarding the usage of GitLab's trademark
* [Authorization Matrix](/handbook/finance/authorization-matrix/) - the authority matrix for spending and binding the company and the process for signing legal documents
* [Procurement Playbook](https://docs.google.com/document/d/1Hb3S0fZ9CoYpvwusksjSP7vibH4z1ShJE8GX62exLGQ/edit?usp=sharing)

{::options parse_block_html="false" /}
