---
layout: handbook-page-toc
title: Professional Services
---

{::options parse_block_html="true" /}

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc}

- TOC
{:toc}

## Professional Services

## How Do I Purchase Professional Services and/or Hire a Contractor?
- If you are looking to work with a new service provider, review the policy for selecting new suppliers on the "Vendor Management" page.
- If you would like help using an existing agency to find a temporary contractor, complete the [GitLab Intake Form](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issue%5Bmilestone_id%5D=#)
    - The Procurement Team will review your request and respond with next steps in 1-2 business days.
- Do NOT agree to ANY business, legal, and or pricing prior to engaging the procurement team. 
    - This ensures alignment across the multiple functions to make good-faith commitments to vendors and don’t put our company in to possible risk.

## How Do I Submit a Request for Professional Services/Contractor?
- Once you've chosen your vendor and have followed the Bid Requirements per above, follow these instructions:
1. [Login to Coupa](https://about.gitlab.com/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-access-coupa) via Okta.
1. Select your supplier
    - If this is a **new supplier**, go to "Forms" in the upper right of your screen, and select "New Supplier Request" form and complete the required fields. Click "Review".
        - Allow 1-2 business days for processing new supplier.
    - If this is an **existing supplier** (or once your new supplier is active in Coupa) select "Professional Services Request" Form.
1. Enter the required Fields and "Add to Cart"
1. Click on your Cart in the upper right, complete remaining fields. Be prepared to include the following:
    - Whether or not the vendor will have access to [red and/or orange data](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html#data-classification-levels) 
    - All data and systems the vendor will have access to 
        - Failure to complete this field will significantly delay the review and approval of your request.
    - Vendor’s Security Contact Email (oftentimes this is different from the main contact. Providing the head of security contact info from the vendor’s company will expedite the security review and avoid delays)
    - Upload any contracts you've received.
    - Click "Submit for Approval"
1. If the answer to any of the questions requires additional review from the GitLab legal team, Procurement will add Labor Law Attorney to the approval flow.

## What Happens after I submit my Request?
    - The Coupa Req will appear as "Pending Buyer Action", this means it is under review from the procurement team. 
    - If the contract requires negotiation, the req will stay in “Pending Buyer Action” status and a "Negotiation" Label will be added to the Coupa Req. 
        - Within 1-2 business days a member of the procurement team will either reach out a-sync, and/or schedule a meeting to review depending on the complexity.
    - If your contract does not require negotiation, the Procurement team will review and ping you directly in the Coupa request.
    - If the anwser is "yes" to any of the required fields in the intake form, procurement will manually add employment law attorney to the Coupa review flow for approval.
    - Once neotiation is completed and/or it is determined that negotiation is not needed, your purchase request will be approved by procurement and will advance to the next approver, which is typically fp&a. 

## How Long Does it take to get my contract reviewed and approved?
- Contracts greater than $25K are typically negotiated by the procurement team, unless you've already done so.
    - The negotiation, review and approval cycle typically takes 2-3 weeks based on the scope of the request. Large and/or complex contracts can take longer to negotiate.
- Contracts less than $25K do not need to be negotiated by procurement unless there is a business reason to do so. 
    - If no negotiation is required, the review and approval cycle typically takes 1-2 weeks. Large and/or complex contracts can take longer, and this can vary wildly based on supplier input into the proces.
- The procurement process consists of multiple approvals from different cross-functional teams, this can include finance, IT, security, and legal. 
- All approvals are necessary before your contract can be signed for compliance purposes. 

## What if I have an Urgent Request?
- Urgent requests that need approval in less than 5 business days, need to be escalated in the #procurement channel for expediting per the below.
- In your slack message you MUST include:
    - Link to your Coupa Request 
        - Contracts should not be posted directly in slack
    - Date needed
    - Specific and quantifiable impact to the business if date is missed. 
        - "Supplier wants it signed today" does not qualify as a reason for escalation and these requests will be denied. 
        - "Price will increase $45K if not signed by Friday" or "Material negative brand impact if not signed by Friday due to missed PR deadlines" are specific, tangible, business impacts, that will be reviewed.
- Urgent requests will be evaluated. Please note these are disruptive to our workflow and our ability to meet SLA's for requests opened on time.
- We may or may not be able to accommodate your urgent request based on the risk and bandwidth available.
- If you have a critical request, **enter the request into Coupa 1-2 weeks prior to needing approval** to avoid needing escalation.
