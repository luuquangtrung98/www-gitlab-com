---
layout: handbook-page-toc
title: Learning Initiatives
description: "The Learning & Development team is rolling out learning programs to enable a culture of curiosity and continual learning."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Learning Initiatives Introduction

The Learning & Development team is rolling out learning programs to enable a [culture of curiosity and continual learning](/handbook/values/#growth-mindset). The learning initiatives are intended to provide a forum for team members to come together to learn about all of the activities taking place in the learning space, as well as hear from leaders internally and externally on various leadership topics.

## Learning Sessions

### Live Learning

Live Learning sessions will be conducted on a monthly basis. There will be a Zoom video conference set up for each session. Official dates and topics will be added to the schedule below as confirmed. If you were unable to attend a live learning session but still want to learn, check out our past live learning sessions below. At GitLab we [give agency](/handbook/values/#give-agency), but if you are attending Live Learning sessions, you will be asked to be engaged and participate with your full attention.

<details>
  <summary markdown='span'>
    Format for Live Learning Sessions
  </summary>

<b>Format for 25 minute sessions:</b>

<ul>
<li>10 minutes - introduction/content</li> 
<li>10-15 minutes - Q&A</li>
</ul>

<b>Format for 50 minute sessions (times below are approximate):</b>

<ul>
<li>10-15 minutes - introduction/content</li>
<li>10-20 minutes - breakout session</li>
<li>10-20 minutes - debrief</li>
<li>5 minutes - conclusion</li>
</ul>

</details>

<details>
  <summary markdown='span'>
    Live Learning Schedule
  </summary>

<b>The upcoming Live Learning schedule is as follows:</b>

<ul>
<li>January - 3 Week Manager Challenge</li>
</ul>

</details>


<details>
  <summary markdown='span'>
    Past Live Learning Sessions
  </summary>

<b>2020</b>

<ul>
<li>January - <a href="https://youtu.be/crkPeOjkqTQ">Compensation Review: Manager Cycle (Compaas)</a></li>
<li>January - <a href="/company/culture/inclusion/being-an-ally/">Ally Training</a></li>
<li>February - <a href="/handbook/people-group/guidance-on-feedback/#receiving-feedback">Receiving Feedback</a></li>
<li>June - <a href="/handbook/people-group/guidance-on-feedback/#guidelines-for-delivering-feedback">Delivering Feedback</a></li>
<li>June - <a href="/company/culture/inclusion/unconscious-bias/">Recognizing Bias</a></li>
<li>September - <a href="/handbook/people-group/learning-and-development/manager-challenge/#pilot-program">Manager Challenge Pilot</a></li>
<li>November - <a href="https://www.youtube.com/watch?v=WZun1ktIQiw">Belonging</a></li>
<li>November - <a href="/handbook/leadership/emotional-intelligence/psychological-safety/#one-week-challenge">One Week Challenge - Psychological Safety</a></li>
<li>December - <a href="/handbook/leadership/coaching/#introduction-to-coaching-1">Introduction to Coaching</a></li>
</ul>

<b>2019</b>

<ul>
<li>November - <a href="/company/culture/all-remote/effective-communication/">Communicating Effectively & Responsibly Through Text</a></li>
<li>December - <a href="/company/culture/inclusion/being-inclusive/">Inclusion Training</a></li>
</ul>

</details>


### Social Learning Through Live Learning

Social Learning is the cornerstone for how L&D designs, develops, and delivers live learning sessions. Social learning focuses on how team members interact with peers for just-in-time learning and skill acquisition through knowledge retention. Live learnings serve as an opportunity for team members to build relationships and a sense of community with team members. Social Learning occurs when team members come together in a virtual forum synchronously to learn from others through networking, breakout groups, storytelling, lessons learned reflection, and collaboration on solving scenarios with role playing. The live learnings enables learners to pull knowledge fro experts and peers within the organization instead of having knowledge pushed to them. 

Example of a Social Learning Live Learning Session on [Building High Performing Teams](/handbook/leadership/build-high-performing-teams/)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/aezVF1nOBWc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

Social Learning can also occur in GitLab's Learning Experience Platform - [GitLab Learn](https://gitlab.edcast.com/) and asynchronous forums using GitLab. (i.e. [Manager Challenge](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/26))

## Learning & Development Monthly Announcements

In FY22 Q1 and Q2, the L&D team noticed low attendance and participation in the team's group conversations. These monthly announcements are an interation on the group conversations. The goals of these announcements include:

1. Announce upcoming L&D programs for team members
1. Field async questions about L&D and from the team
1. Increase participation from team members in all time zones

### Monthly Announcement Strucutre

Monthly Announcements from the L&D team are a 3-part announcement. This includes:

1. Async AMA issue for sharing upcoming opportunities, key results, and threaded discussion
2. Short, 3 minute video recorded using Loom and shared via Slack

Future iterations of the announcement will include:

1. Using Bananatag to share the content via email with the team and Email that highlights opportunities and results
1. Leveraging relationships with managers and PBPs to share in division specific channels

### Preparation checklist for GitLab team members

1. Record short video using Loom that highlights 2-3 opportunities coming up and 1-2 key results from the previous month
1. Open and populate an issue using the `monthly-announcement` template. Collaborate with L&D team to include all important events in this issue body.
1. Post video, link to slides, and link to AMA issue in Slack on the first Tuesday of the month in the #whats-happening-at-gitlab Slack channel
1. Share the post from the #whats-happening-at-gitlab channel to 
     - the #ceo channel and tag Sid
     - the #managers channel and ask managers to share with their teams
     - the PBPs and ask to amplify with division leadership

### Schedule

Monthly announcements are shared by the L&D team on the first Tuesday of each month. Below, you'll find links to previous announcement materials.

| Month | Video Link | Async AMA | Total Views | Total Comments |
| ----- | ----- | ----- | ----- | ----- |
| 2021-09 | [Video](https://www.loom.com/share/a087f45f78134f04a8260fd181cead53) | [Issue](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/305)| 44 | 1 |
| 2021-10 | [Video](https://www.loom.com/share/eee099e204a54769a050babee0b67c6c) | [Issue](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/324) | 32 | 0 |
| 2021-11 | - | - | - | - |
| 2021-12 | - | - | - | - |
| 2022-01 | - | - | - | - |


### Results

The following metrics will be used to measure results of the L&D monthly announcements

| Metric | Monthly Goal |
| ----- | ----- | 
| Total Loom Video Views | 20% of company | 
| Total comments/questions posted on the issue | 10 | 


## Learning Speaker Series Overview

The L&D team hosts a monthly Learning Speaker Series call, open to all team members. The calls serve to provide a space where senior executives and mid-level people leaders can share relevant topics to engage and teach team members lessons learned from their own careers and answer relevant L&D related questions. We also hope to use this forum for external speakers to teach topics on relevant skills for team members.

### Goals

1. Provide a forum where team members can hear from established leaders on how they've managed their career and lessons learned to share with the wider community.

### Examples of Call Topics

This list of topics can be used as a starting point for brainstorming content and connecting with individuals to feature during Learning Speaker Series calls. This is not an exhasutive list - please add new ideas to this list as they come up in each monthly call!

- L&D Initiatives
- Leading Teams
- Managing Remote Teams
- Managing Underperformance
- Crucial Conversations
- Coaching
- Leadership Best Practices
- Developing Emotional Intelligence
- Developing High Performing Teams
- Managing Conflict

### Past Learning Speaker Series calls

Check out recordings of previous Learning Speaker Series calls!

- 2020-11-19 [Building Trust with Remote Teams Learning Speaker Series](https://www.youtube.com/watch?v=hHMDY77upAE&feature=youtu.be)
- 2020-12-10 [Managing Burnout with Time Off](/company/culture/all-remote/mental-health/#rest-and-time-off-are-productive)

### Hosting a Learning Speaker Series call

The following process outlines steps for the L&D team to take when planning and implementing the calls.

Anyone or any team can host a Learning Speaker Series for the organization! If interested and there is a topic & speaker lined up, please fill out a [Learning & Development Request template](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/blob/master/.gitlab/issue_templates/learning-and-development-request.md) to begin the process.

**Planning Issue Template:** Open a `learning-speaker-series` issue template in the L&D [General Project](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/blob/master/.gitlab/issue_templates/learning-speaker-series.md) to host a session. Steps outlined below are also included in the issue template and are included here as reference.

1. Collaborate with exec team, mid-level people leaders, and other interested GitLab Team members to gauge interest and availability to be featured in the call. Be mindful to include diverse voices from across the GitLab team.
1. Determine if this is an internal or external speaker for the series call. If external speaker, ensure that the speaker has been validated and that the topic covered aligns with our values. External speaker presentations need to aling with how we deliver group conversations.
1. Post in the [#people-connect Slack channel](https://app.slack.com/client/T02592416/C0SNC8F2N/thread/C0SNC8F2N-1602618225.269200) to coordinate a Zoom meeting on the company calendar. This meeting should be set up to live stream to the GitLab unfiltered YouTube channel. Consider hosting the call at least 2 times to best accommodate for multiple time zones.
1. Test Zoom functionality with the speaker at least two business days before event.
    - If external speaker, ensure they have downloaded [Zoom](https://zoom.us/support/download).
    - Have speaker test [audio and video](/handbook/tools-and-tips/zoom/#how-to-test-audio-and-video-in-zoom).
    - Check that speaker will be able to [share a presentation](/handbook/tools-and-tips/zoom/#how-to-share-a-presentation-in-zoom) if necessary.
1. Send speaker calendar invite with all details for the call (including, but not limited to, Zoom link, agenda, slides, etc.).
1. Create slide deck for presentation. Make a copy of a previous month's presention in the [Continuous Learning Campaign Google Folder](https://drive.google.com/drive/folders/1d4ksJXBMrATATxN0QyJ4FA6hzchMNdvb?usp=sharing)
1. Open a feedback issue for questions and comments to be shared asynchronously.
1. Coordinate an announcement of the call in the #company-fyi Slack channel from Sid or another executive/manager who will be featured that month. The post should be shared 1 business day before the call. This post should include a link to the slide deck and coresponding issue. See notes below for a template that can be shared.

#### Text for CEO share in #company-fyi channel

`Join the Learning and Development team on [DATE] for the Learning Speaker Series Call. This month's call features [PERSON] and their experience with [BACKGROUND}. You can review the slide deck for the call [HERE], and post questions you might have in the call adenga doc [HERE]. Looking forward to seeing you there!`

## Learning & Development Quarterly Newsletter

The L&D team also hosts and develops a [quarterly newsletter](/handbook/people-group/learning-and-development/newsletter/) for the community.

## Mental Health Quarterly Newsletter

The L&D team publishes a [quarterly mental health newsletter](/handbook/people-group/learning-and-development/newsletter/mental-health-newsletter) for team members and the wider community.

## Take Time Out To Learn Campaign

[Focus Friday's](/handbook/communication/#focus-fridays) are a great benefit at GitLab. We try to schedule minimal meetings on Fridays to catch up on work and grow our skills professionally. Use Focus Fridays to take time out to learn. Team members can work with their manager through a [career development conversation](/handbook/leadership/1-1/#career-development-discussion-at-the-1-1) to determine what skills that want to grow in the future. Aspirations can be documented in an [individual growth plan](/handbook/people-group/learning-and-development/career-development/#internal-resources). 

From there, identify what will be needed to attain the new skills and consider using the [Growth and Development Benefit](/handbook/total-rewards/benefits/general-and-entity-benefits/#growth-and-development-benefit) or [expensing professional development opportunities such as coaching, worskshops, conferencces, self-service learning, etc.](/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development/#types-of-growth-and-development-reimbursements). Another option you can utilize to learn more is [LinkedIn Learning](/handbook/people-group/learning-and-development/linkedin-learning/). GitLab has a number of LinkedIn Learning licenses for team members. Block time off your calendar every week or month to devote to learning new skills. Share what you are learning with team members and in the #learninganddevelopment slack channel. 

In a [discussion with Darren Murph, Head of Remote at GitLab, about his career development](https://gitlab.edcast.com/pathways/career-development-with-darren-murph/cards/7123731), Darren called out the importance of taking time out to learn, reminding the team that career development is "not something that comes around the fringes of work - it is work". The Take Time out to Learn initiative leans into this idea.

Consider documenting the steps you are going to take learn new skills in the individual growth plan. Check in with your manager and ask for accountablity from them to help you stay aligned with goals. 


## Mini-Challenge

Mini-challenges are 1 week learning opportunities that utilitze a combination of GitLab issues, live learning sessions, and Slack to organize a group of learners around a specific topic. Mini-challenges are advertised to team members via slack and require interested learners to opt in. Participation in issue discussion questions and in live learning sessions (synchronous and asynchronous) are is tracked by the L&D team or challenge organizer. Upon completion of the challenge, participants are awareded a certification. The L&D team has used the mini-challenge model to engage learners on topics like psychological safety.

In addition to the use of GitLab, each mini-challenge includes a dedicated Slack channel to organize conversation, share new issue links, and provide space for team members to ask questions.

Examples of current and past challanges can be found in the [Learning and Development project](https://gitlab.com/gitlab-com/people-group/learning-development/challenges)

### GitLab Mini and Extended Challenges

All of GitLab's Learning and Development programs use the [GitLab tool to facilitate learning](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/1). One way we do that is with the use of daily challenges. Daily challenges are a proven strategy for implementing new habits for our team members. Daily challenges can cover a range of topics, and they are intended to introduce behavior changes that will stick. In September of 2020, we launched a [Four Week Manager Challenge program](/handbook/people-group/learning-and-development/manager-challenge/) that consisted of daily challenges and live learning events to strengthen foundational management skills. In November of 2020, we launched a [One Week Challenge on Psychological Safety](/handbook/leadership/emotional-intelligence/psychological-safety/#one-week-challenge). 

**Sample structure of a challenge:**
- Length of challenge: 1 week (But can be for as long as 4 weeks)
- Sample challenge topic: Giving Feedback
- Daily challenges: 20-minute daily challenge (Monday through Friday)
- Live Learning: Optional live learning session at the end of the week to revisit the challenges covered
- Self-Reflection: At the end of the week, have participants complete a self-reflection exercise
- Certification/Badging: Option to award participants with a certification or badge

## Internal Learning Campaigns

Learning campaigns at GitLab have an asynchronous focus and are used to raise awareness around a specific topic or set of resources. These campaigns are inspired by the structure of a learning challenge but without the required engagement or tracking. For example, the L&D team is using a learning campaign structure to host a [mental health awareness week](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/156)

Learning campaigns use a combination of GitLab issues and Slack announcements to raise awareness and spark discussion. Following the campaign, the L&D team will aggregate comments and resources that have surfaced and document in the handbook. Learning campaigns can be used to build buzz around a live speaker series or to start conversation about a common question or issue that GitLab team members are facing.

### Management Tip of the Week

Weekly Slack messages that are posted in the #managers channel with quick, practical management advice to help people leaders do their job better, delivered weekly on Thrusday's. Sample topics include:
- LinkedIn Learning Courses, articles, GitLab Learn, handbook pages, etc. 
- Training announcements
- External webinars
- Management thought leadership


## CEO Handbook Learning Sessions

GitLab's Handbook pages grow every day. Each page serves as GitLab's primary source of [learning and development material](/handbook/people-group/learning-and-development/#handbook-first-training-content). Throughout FY22, the L&D team and the CEO will hold recorded interactive learning sessions to analyze Handbook pages. The goal of the sessions will be to incorporate more video-based learning into the handbook.

Three types of CEO handbook learning sessions:

1. **Handbook Readout:** Bit-sized recording where the L&D team and the CEO review what is on the page. (5 minute video)
1. **Handbook Discussion:** Interactive discussion where L&D facilitates an engaging conversation with the CEO and e-group members. We openly discuss the concepts on the page and allow senior leaders to share best practices implementing them. (25 minute video)
1. **Handbook Interview:** The Learning and Development team will create new pages, create sub-pages, or update existing pages based on interview topics discussed during a targeted Q&A. The video will serve as the foundation for new learning content at GitLab.

The video below from Sid and L&D further explains the concepts:
<figure class="video_container"><iframe src="https://www.youtube.com/embed/cB301JaYnsw" height="315" width="560"></iframe></figure>Steps for L&D Team when setting up a CEO Handbook Learning Session:

Preparing for the Call: 

1. Review topic relevant Handbook pages.
1. Determine what topics you want to cover each week
1. Research the respective page that will be discussed or section of a page
1. Complete some work on the page before the session if you can, create an MR that clarifies information or asks Sid for clarity on the topic discussed.
1. Set up a meeting with CEO through EBA. Invite other e-group members if applicable.
1. Create an agenda with talking points and areas to emphasize during video recording with CEO
1. Send Sid the agenda in the #CEO channel at least 24 hours in advance 
1. Hold a handbook learning session and ensure the discussion is fluid and interactive with open and honest dialogue.

During the Call: 

1. Give Sid and other speakers, if applicable, a few minutes at the beginning of the call to populate answers. 
1. Once everyone is ready, stream to YouTube Live. 
1. Make the title of the agenda, the same as the YouTube Video. 
1. Do a quick introduction. 
1. Go through the questions on the agenda. 
1. If questions come up during the interview, add them to the end of the agenda if there is time
1. When there’s nothing on the agenda left, thank him for his time and end the live stream. 

After the Call:  

1. Determine if L&D can create bite-sized videos with the content. Post bite-sized and long-form video on YouTube Unfiltered channel
1. Update Unfiltered page with more information after the Live Stream. See the the description in this [example video](https://www.youtube.com/watch?v=_okcPC9YucA).
1. Edit the live stream thumbnail with a [Title Page](https://docs.google.com/presentation/d/13Pj3bxYK6FFSsiMx2vOAcsB0C6ctl_1lqFH6E5MtgGE/edit#slide=id.gb1c2c2e157_0_115) for each topic. Here is [the template](https://docs.google.com/presentation/d/13Pj3bxYK6FFSsiMx2vOAcsB0C6ctl_1lqFH6E5MtgGE/edit#slide=id.gb1c2c2e157_0_115) for the videos. Write out a new title, screenshot the image, and update thumbnail. 
1. Embed video on the related Handbook page.
1. Edit page with new content, create sub-page, send MR in the CEO channel with a proposal for an interactive handbook discussion.

List of CEO Handbook Learning Sessions:

1. [Common misperceptions about Iteration](https://www.youtube.com/watch?v=nXfKtnVUhvQ)
1. [No Matrix Organization](https://www.youtube.com/watch?v=E_wegGRv4mA)
1. [Making Decisions](https://www.youtube.com/watch?v=-by6ohMIi_M&feature=emb_title)
1. [Individual Contributor Leadership](https://www.youtube.com/watch?v=d0x-JH3aolM)
1. [Bias Towards Asynchronous Communication](https://www.youtube.com/watch?v=_okcPC9YucA&feature=emb_title)
1. [High Output Management](https://about.gitlab.com/handbook/leadership/high-output-management/#applying-high-output-management)
1. [Giving and Receiving Feedback](https://www.youtube.com/watch?v=vL864Zg2sm4&t=731s)
1. [Managing Underperformance](https://www.youtube.com/watch?v=-mLpytnQtlY&t=637s)
1. [Transitioning from IC to Manager - Engineering](https://www.youtube.com/watch?v=Zeull-tdy6o) 
1. [Manager Mention Merge Requests](https://www.youtube.com/watch?v=e1sTOtveNOk)
1. [Working Groups](/company/team/structure/working-groups/#ceo-handbook-learning-discussion-on-working-groups)
1. [Skip Level Meetings](https://www.youtube.com/watch?v=kAxp0Mam-Rw)
1. [Product Strategy](https://www.youtube.com/watch?v=yI29xFAgKoA)
1. [Mental Wellness Discussion](https://www.youtube.com/watch?v=od_KdZqc69k)

## Skill of the Month 

The L&D team outlines a skill of the month for team members to learn more about a particular topic. This provides team members with direction about what topics to learn more about and where to find information on these topics. This initiative begins in April 2021. 

### Goals

1. Get team members engaged with our learning platforms (GitLab Learn and LinkedIn Learning). 
1. Provide opportunities for Growth and Development. 
1. Encourage team members to take time out to learn. 

### FY22 Topic Outline

This is the list of topics that will be the focus in FY22. Each skill of the month can be found in the [Skill of the Month (FY22)](https://gitlab.edcast.com/channel/skill-of-the-month-fy22) channel on [GitLab Learn](https://gitlab.edcast.com/). 

- April: Managing Stress
- May: Effective Communication
- June: Coaching
- July: Culture of Feedback
- August: Career Development
- September: Manager of One
- October: Emotional Intelligence
- November: [topic TBD]
- December: [topic TBD]
- January: [topic TBD]

### Organizing a Skill of the Month 

The following process outlines steps for the L&D team to take when planning and implementing the Skill of the Month.

Anyone or any team can recommend a topic for the future! If interested, please fill out a [Learning & Development Request template](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/blob/master/.gitlab/issue_templates/learning-and-development-request.md) to begin the process.

**Planning Issue Template:** Open a `skill-of-the-month` issue template in the L&D [General Project](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/tree/master). Steps are outlined in the issue template.

## People Group Self-Care Check in Calls

This optional call occurs weekly on Wednesday and is a space for the People Group to check-in weekly on our own self-care practices. This initiative is currently in a pilot state and we're iterating on the best format. Other teams at GitLab should feel encouraged to adpot a similar call format if they are interested. [Calm](https://www.calm.com/business/blog) offers great blogs and articles about self-care strategies, and a webinar they hosted in March 2021 inspired the start of these weekly social self-care calls.

This call does not have an agenda because the intention is to hold a safe space where peole can speak openly without focusing on documenting discussions.

Call Structure:

- First 10 minutes: Optional share-out of what you've done this week or this day to care for your self. Participants can share verbally or in the Zoom chat
- Second 10 minutes: Discuss one question prompt about self-care, mental wellness, and general wellbeing. The goal is to get participants thinking about how they can integrate self-care strategies into their daily practice

### Example self-care discussion questions

1. What have you managed well in the past year?
1. What brought you joy today? This week? This month?
1. How do you feel at this moment? Why?
1. What advice do you give others? Do you follow it for yourself?
1. When the people I spend time with do ____ or say ____, I feel energized.
1. How do you move past unpleasant thoughts, experiences, or discussions?
1. What do you do that makes you feel most like yourself? Why?

## Crucial Conversations

Two members of the GitLab Learning & Development Team are certified to deliver Crucial Conversations Training. You can learn more about the importance of Crucial Converations at GitLab on our [Crucial Conversations](/handbook/leadership/crucial-conversations/) handbook page. 

If you complete Crucial Conversations training with one of our in house certified trainers, you can utilize our [growth and development budget](/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development/#crucial-conversations-training). 

### What to Expect 

The video below gives an overview of what the Crucial Conversations training looks like. 

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/UX-ArZJJJ1U" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

A detailed training outline is included below:

**Total program length:** 4 Weeks
**Time commitment per week:** Approx 2 hours self-paced and 1 hour live practice session

| Week Number | Topic | Commitment |
| ----- | ----- | ----- |
| 1 | Get Unstuck, Master my Stories 1, and Master my Stories 2 | 3 hours |
| 2 | Start with Heart, State my Path | 3 hours |
| 3 | Make it Safe, Learn to Look, and Seek Mutual Purpose | 3 hours |
| 4 | Explore Other's Paths, Move to Action | 3 hours |

Training weeks will run Wednesday-Wednesday, with 1 live trainings hosted each week. Upon completion of the 4 week training, you will earn an official Crucical Conversations certification from Crucial Learning.

### Upcoming Sessions 

If you are a GitLab Team Member and interested in participating in the course, you can fill out our [Crucial Conversations interest form](https://docs.google.com/forms/d/e/1FAIpQLSdqwibbQZs-zL-IX9aq9Yzgozm-y3i0Vwh59T8T1nR74mxmFQ/viewform). 

**FY22** 

- 2021-12: Security department course
- FY23-Q1: Exact dates to be confirmed

### Setting up the Training 

1. Use the Crucial Conversations [Course Manager Tool](https://training.vitalsmarts.com/learn/sign_out?client=gitlab) and [Course Manager Guide].
1. Ensure we have a sign up link from crucial conversations. 
1. Once we have the link, send the invite email to all team members interested in the program. 
1. Add all cohort participants to the [#crucial-conversations](https://app.slack.com/client/T02592416/C0258087472) Slack channel
1. Add all participants to the live event calendar invites 
1. Create a subfolder in the [Crucial Conversations](https://drive.google.com/drive/u/1/folders/144sRv0ap4Gwp4IcM_mtkK83c4toVGJZJ?ths=true) Google Drive to organize cohort materials
1. Use the [live session template](https://docs.google.com/presentation/d/1cXLjK_9_7ndngmgW_5z4yKcLx7iCxZSNgvEVZ7fNJEs/edit?usp=sharing) to organize GitLab-customized Crucial Conversation scenarios for each live session

## New team member Slack training

This training is a 10 day, automated message training via Slack to introduce new team members to key strategies for using Slack at GitLab. All content in the training is based on documentation in the [Slack handbook](/handbook/communication/#slack).

[Learn more about the Slack training and sign up here](/handbook/people-group/learning-and-development/learning-initiatives/slack-training).

Please note: Existing team members are welcome to sign up for the new team member Slack training, as long as they understand the content is directed to brand new Slack users.

## Past Initiatives


### Monthly Continuous Learning Call Overview

The Learning and Development (L&D) team previously hosted a monthly Monthly Continuous Learning call. The series was intended to showcase all of the L&D initatives taking place at GitLab as well as provide a space for team members to ask questions. The series was replaced in FY22 with involvement in company-wide Group Conversations in an effor to increase participation and attendance in the calls.

#### Goals

1. Spur a culture of curiosity and continuous learning by highlighting all the initiatives we have taking place in learning & development.
1. Increase engagement and improve team member satisfaction and productivity by advancing understanding of available L&D resources

Additional background information on this initiaitve is available in the [Monthly Continuous Learning Call issue](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/96)

#### Past Monthly Continuous Learning Call

Check out recordings of previous Learning Speaker Series calls!

- [2020-10-21 Monthly Continuous Learning Call](https://www.youtube.com/watch?v=nfF9OI566uo&list=PL05JrBw4t0Kqz6UXFu_2ELLubpKzg_pOe&index=32)
- [2020-11-18 Monthly Continuous Learning Call](https://youtu.be/tWZ3iVyb-4E)
- [2020-12-16 Monthly Continuous Learning Call](https://www.youtube.com/watch?v=DQdzXFu9Nbc)
- [2021-01-20 Monthly Continuous Learning Call](https://www.youtube.com/watch?v=pnc4FqHk_a0)

#### Hosting a Monthly Continuous Learning Call

The following process outlines steps for the L&D team to take each month when planning and implementing monthly calls.

**Planning Issue Template:** Open an Monthly Continuous Learning Call issue template in the L&D Project. Steps outlined below are also included in the issue template and are included here as reference.

1. Post in the [#people-connect Slack channel](https://app.slack.com/client/T02592416/C02360SQQFR) to coordinate a Zoom meeting on the company calendar. This meeting should be set up to live stream to the GitLab unfiltered YouTube channel. Consider hosting the call at least 2 times to best accommodate for multiple time zones.
1. Create slide deck for presentation. Make a copy of a previous month's presention in the [Continuous Learning Campaign Google Folder](https://drive.google.com/drive/folders/1d4ksJXBMrATATxN0QyJ4FA6hzchMNdvb?usp=sharing)
1. Coordinate slide deck with appropriate enablement audiences (i.e DIB, Field Enablement, Professional Services, Marketing Enablement, etc)
1. Update slide deck for presentation with feedback from coordinated audiences
1. Open a feedback issue for questions and comments to be shared asynchronously.
1. Coordinate an announcement of the call in the #company-fyi Slack channel from Sid or another executive/manager who will be featured that month. The post should be shared 1 business day before the call. This post should include a link to the slide deck and coresponding issue. See notes below for a template that can be shared.

##### Text for CEO share in #company-fyi channel

`Join the Learning and Development team on [DATE] for the Monthly Continuous Learning Call. This month's call is all about [TOPIC]. You can review the slide deck for the call [HERE], and post questions you might have in the call adenga doc [HERE]. Looking forward to seeing you there!`
