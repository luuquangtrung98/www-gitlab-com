---
layout: markdown_page
title: "FY23-Q1 OKRs"
description: "View GitLabs Objective-Key Results for FY23 Q1. Learn more here!"
canonical_path: "/company/okrs/fy23-q1/"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from February 1, 2022 to April 30, 2022.

## On this page
{:.no_toc}

- TOC
{:toc}

## OKR Schedule
The by-the-book schedule for the OKR timeline would be

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
| -5 | 2022-03-28 | CEO shares top goals with E-group for feedback |
| -4 | 2022-04-04 | CEO pushes top goals to this page |
| -4 | 2022-04-04 | E-group propose OKRs for their functions in [Epics and Issues nested under the CEO's OKRs](/company/okrs/#executives-propose-okrs-for-their-functions). These issues and epics are shared in #okrs Slack channel |
| -3 | 2022-04-11 | E-group 50 minute draft review meeting on 2021-10-18 |
| -2 | 2022-04-18 | E-group discusses with their respective teams and polish their OKRs |
| -1 | 2022-04-25 | CEO reports post links to final OKR Epics in #okrs slack channel and @ mention the CEO and CoS to the CEO for approval |
| 0  | 2022-05-02 | CoS to the CEO updates OKR page for current quarter to be active |


## OKRs

### 1. CEO

### 2. CEO

### 3. CEO


