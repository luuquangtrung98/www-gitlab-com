---
layout: markdown_page
title: "TMG - Team Member Group Guide"
description: "An overview of what is needed to start and sustain a Team Member Resource Group or a Team Member Discussion Group"
canonical_path: "/company/culture/inclusion/erg-guide/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

##  Introduction
On this page you will be provided an overview of what is needed to start and sustain a GitLab TMRG (Team Member **Resource** Group) or a Team Member **Discussion** Group.

## Definition of the TMG - Team Member Groups
TMRGs are voluntary, team member-led groups focused on fostering diversity, inclusion and belonging within GitLab. These groups help team members build stronger internal and external connections; offer social, educational, and outreach activities; create development opportunities for future leaders; and increase engagement among team members.

Team Member **Resource Group** provide support for an Underpresented group
- The purpose for this type of TMRG is to develop and elevate that underrepresented group within Gitlab. To provide a voice and a safe space for people who identify as a part of that group and their allies. With aim of increasing and developing the sense of belonging. 

Team Member **Discussion Group** that is for the purposes of discussions and/or allyship 
- The purpose of these groups is to provide either an avenue to perform more meaningful allyship activities and bring together ideas on how to be effective allies to underrepresented groups. An example of this being Men for Inclusion (Name not yet confirmed). This could also be an avenue for groups that won't be able to influence large parts of Gitlab Policy but be able to discuss issues that effect them, for example religion and faith. 

These types of groups must have a much clearer mission and purpose. Allyship groups in particular must be action orientated. Any budget assigned to these groups should be used in support of other TMRGs or Diversity, Inclusion & Belonging initiatives. 

### What is not an TMRG at GitLab
There are many types of groups and not all of them meet the criteria of being a GitLab supported TMRG. Here are some examples of those that would not be considered TMRGs here at GitLab:
* Groups formed for the purpose of opposing other groups
* Groups formed to promote political affiliations
* Groups formed around strictly social activities
* Groups of passion such as those formed around a passion for coffee, exercise, book reading, photography, etc.

**NOTE:** “GitLab supported TMRG” means the group is formally recognized by the company as a GitLab TMRG.

## How to Join Current TMRGs and their Slack Channels

The following groups have completed the process to be an TMRG and received formal support as part of the [DIB framework](https://about.gitlab.com/company/culture/inclusion/#ergs---employee-resource-groups). Click the signup link (GitLab team members only) to join:

Ordered alphabetically to avoid the perception that any TMRG is more important than any other TMRG.

| **TMRG** | **Team Leaders** | **Slack Channel** | **Sign Up** | **Ongoing TMRG Agenda** | Executive Sponsor |
| ------ | ------ | ------ | ------ | ------ |------ |
| [GitLab API - Asia Pacific Islander](/company/culture/inclusion/tmrg-gitlab-api/) | [Tanuja Paruchuri](https://gitlab.com/tparuchuri) [Christopher Wang](https://gitlab.com/cs.wang) [Steve Xu](https://gitlab.com/steve_xu) | #api-tmrg | [Sign up for future meetings (Google group)](https://groups.google.com/a/gitlab.com/g/api-tmrg) | [API Agenda](https://docs.google.com/document/d/17zJuZlWVg40mPjcybnGlj_mK2f4SEpRdrjq3s3Cseow/edit?usp=sharing) | David Hong |
| [GitLab DiversABILITY](https://about.gitlab.com/company/culture/inclusion/erg-gitlab-diversability/) | [Melody Maradiaga](https://gitlab.com/mmaradiaga) [Wil Spillane](https://gitlab.com/wspillane) | #diverse_ability | [Sign up for future meetings (google form)](https://forms.gle/5g2wVB577Lr7M3CH7) | [DiversABILITY Agenda](https://docs.google.com/document/d/130LH9QPewms025z4OFxNNUYnjcQ53D1049EZOL2WdWc/edit?usp=sharing) | TBC |
| [GitLab Generational Understanding](/company/culture/inclusion/tmrg-gitlab-generational-understanding/) | [Wayne Haber](https://gitlab.com/whaber) and [Francis Potter](https://gitlab.com/francispotter) |#generational_understanding | [Sign up for future meetings (Google group)](https://groups.google.com/a/gitlab.com/g/Generational_Differences_ERG) | [Generational Understanding Agenda](https://docs.google.com/document/d/1h81q60KnCJFsATKjZKOFglBmgkv8TlGrKY8punX2SEg/edit?usp=sharing) | Eric Johnson
| [GitLab Latinx](/company/culture/inclusion/tmrg-gitlab-latinx/) | [Pilar Mejia](https://gitlab.com/pmejia) [Hugo Azevedo](https://gitlab.com/hugoazevedo) [Romer Gonzalez](https://gitlab.com/romerg) [Chris Cruz](https://gitlab.com/chriscruz) | #latinx | [Sign up for future meetings (Google group)](https://groups.google.com/a/gitlab.com/g/latinxtmrg) | [Latinx Agenda](https://docs.google.com/document/d/15I-Lp9BO1FgnSCxANPiTemrvbcSLppf_xp8I0vIUbPY/edit?usp=sharing) | Wendy Barnes |
| [GitLab MIT - Minorities in Tech](https://about.gitlab.com/company/culture/inclusion/erg-minorities-in-tech/) | [Aricka Flowers](https://gitlab.com/atflowers) and [Sharif Bennet](https://gitlab.com/SharifATL) |  #minoritiesintech | [Sign up for future meetings (google group)](https://groups.google.com/a/gitlab.com/g/mittmrg) | [MIT Agenda](https://docs.google.com/document/d/1pEuyMTWs-H4Qgi2JozuVdDqb03eEc3onNzltGMoYELI/edit?usp=sharing) | TBC |
| [GitLab Pride](https://about.gitlab.com/company/culture/inclusion/erg-gitlab-pride/) | [Alex Hanselka](https://gitlab.com/ahanselka) and [Helen Mason](https://gitlab.com/hmason) | #lgbtq | [Sign up for future meetings (Google group)](https://groups.google.com/a/gitlab.com/g/pride-tmrg) | [Pride Agenda](https://docs.google.com/document/d/11D-Izm5mbopEUCY036Qr0bHx6UePMyfkzhUfNDE-mnk/edit?usp=sharing) | TBC |
| [GitLab Women](/company/culture/inclusion/tmrg-gitlab-women/) | [Kyla Gradin](https://gitlab.com/kyla) Madeline Hennessy | #women | [Sign up for future meetings (google group)](https://groups.google.com/a/gitlab.com/g/womenstmrg) | [Women Agenda](https://docs.google.com/document/d/1pYbosJKKXJzUCb-2MSWiGKMC94ms2GytQFW7J0YmMVE/edit?usp=sharing) | Robin Schulman |

## Benefits of TMRGs
In general TMRGs are an excellent support system and key to providing awareness, respect, and building diversity, inclusion and belonging within the workplace. These groups are a proven way to increase cultural competency, retention of team members, provide marketplace insights to the business, attract diverse talent, and more.  The endorsement of TMRGs gives team members the opportunity to identify common interests, and decide how they can be shared with others.

### GitLab Benefits of TMRGs
* Grow GitLab’s business and the company from DIB branding as it’s top value which will appeal to customers and attract new team members.
* Support GitLab values and business goals, including the Company’s commitment to foster an inclusive work environment.
* Support GitLab’s diversity initiatives, aspirations and goals.
* Foster communications between GitLab and its team members.
* Provide mentoring and educational and professional development opportunities for GitLab team members

### Team Member Benefits of TMRGs
* TMRGs introduce new and current team members to GitLab's culture and helps to build and maintain engagement.
* TMRGs offer team members togetherness, camaraderie and connections to GitLab giving them a sense of belonging.
* You will have the opportunity to develop your professional and leadership skills, build relationships across the company and regions, connect with possible mentors/mentees, raise awareness for underrepresented groups at GitLab and make a difference at work and in your communities.
* TMRGs provide team members with opportunities for development, showcasing their skills and developing their own brand within GitLab.
* Team members will also have opportunities to obtain mentoring, networking, providing positive impacts to the business, and channeling their voices to advocate for change.

## Forming a TMRG
TMRGs support our diversity, inclusion and belonging framework, maintain an open forum for the exchange of ideas, and serve as a source of educational and professional development opportunities for GitLab team members. It is expected that all GitLab supported TMRGs will participate in initiatives that focus on the following group elements:

1. **TMRG Member Development:** Activities that further the development of group members. Examples could include:
   * Developing and delivering developmental opportunities for members
   * Potential career development events and activities
   * Identifying effective mentoring opportunities
   * Building a network of development resources that are easily accessible by members

2. **Outreach/Business Development:** Connecting with groups beyond GitLab
   * Establishing internal and external business partnerships
   * Representing GitLab at industry events
   * Working with external communities to help GitLab achieve market presence and leadership brand

3. **Awareness and Education:** Raising awareness and educating all team members.
   * Events/Initiatives that bring awareness and education of the TMRG to the company
   * Engagement of Allies and GitLab team members with the TMRG

4. **Talent Acquisition/Retention:** Promoting, growing, and developing the TMRG group as a whole.
   * Establishing partnerships with universities and or STEM programs
   * Working with Talent Acquisition to identify sourcing and talent acquisition opportunities
   * Creating initiatives that attract related diversity dimension talent.

Possible events:
* Webinars
* Panel discussions
* Effective Presentations
* Effective Communications
* Virtual Lunch n Learns
* Speed Networking with Executives
* “Meet the TMRG” (informational outreach event for employees to learn more about the DIB Community)
* Volunteer activities (could be co-sponsored with other TMRGs)
* Recognition events (ex.  A meeting to celebrate the TMRG annual achievements, award success of individuals)

## How to suggest a new TMRG at GitLab?

* To suggest a new TMRG at GitLab please use the [TMRG Request Issue Template](https://gitlab.com/gitlab-com/diversity-and-inclusion/-/blob/master/.gitlab/issue_templates/TMRG-request-template.md).

While creating the new issue please:
* Take an opportunity to fully read the guide here provided
* Be prepared to discuss and explain the TMRG diversity dimension that you would like to start.
* Provide a detailed explanation of why GitLab should support the TMRG
* Provide a draft of a possible mission statement for the TMRG

## Let's Get Started

### Naming and branding for the TMRG

All names, because they are visible externally and could compete with other projects, products and or not be a good representation of GitLab must be approved by Legal and Brand.  You should work with the DIB Manager (Candace Williams cwilliams@gitlab.com) to gain a consensus on ideas.  Keep in mind that names chosen by the TMRG may not meet GitLab’s naming and branding standards and may need to be changed.

### Defining your TMRGs mission statement
A mission statement is the simplest and clearest way to explain the purpose of your group and how it will achieve its goals. Keep your mission statement short, and use simple terms that everyone understands. Finally, make sure the mission is flexible enough to allow for goals and activities to change over time. Below are some examples of mission statements used by similar groups at other companies:
* Deutsche Bank - Rainbow Group Americas: The DB-Rainbow Group Americas is an organization open to all Deutsche Bank employees to promote an inclusive and productive work environment for gay, lesbian, bisexual, and transgender (GLBT) employees to enhance their professional and personal development in alignment with business objectives.
* General Mills - Black Champions Network: To champion the growth, development and success of all African American employees while maximizing their contribution to General Mills’ goals and objectives.
* ING - Latino Network: The ING Latino Network fosters its members’ development and promotes  cultural awareness within ING’s corporate strategies and objectives.
* Salesforce - Women’s Network:  We are dedicated to building gender Equality in the workplace and beyond through empowering, supporting, and investing in our global community for women and their allies. We are the largest Ohana group with 6000+ members across 30+ hubs globally. Our programs include LeanIn Circles, volunteer opportunities, International Women’s Day events, Woman of the Month series, mentorship opportunities, children’s initiatives, and Women in Technology programs. Our focus is always on improving inclusion and Equality for all on the gender spectrum, to help make Salesforce the best place to work for all.
* Texas Instruments Incorporated—Chinese Initiative: The mission of the Chinese Initiative is to create a work culture in which all people are valued, empowered, and given opportunities to develop and contribute to their full potential, thereby gaining a competitive advantage for Texas Instruments.

### Create a project

As with all GitLab business, we want to dogfood our own product. As such, you should consider creating a GitLab project to manage discussions in issue and
update the repo with mission statement, events, and the like. You should create the repo in the [`gitlab-com`](https://gitlab.com/gitlab-com) group. To see
a project in action, you can check out the [GitLab Pride project](https://gitlab.com/gitlab-com/pride-erg).

### Create a Google group

Managing membership will be greatly simplified by using a Google group. The main benefit is that you can invite the group to any calendar events and users can
join or leave the group on their own. In order to create a Google group, you'll need to create an [access request issue](/handbook/business-ops/team-member-enablement/onboarding-access-requests/access-requests/#slack-google-groups-1password-vaults-or-groups-access-requests)
requesting a new group. There is a template you can use and you can [view an example](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/3547)
issue if you'd like. Once you've got the Google group created, you can add users manually or allow them to sign up on their own at the group homepage. You can
look at the [pride-erg](https://groups.google.com/a/gitlab.com/forum/#!forum/erg-pride) for an example of what that might look like.

## TMRG Group Members
Membership in an TMRG at GitLab is open to everyone, including full-time and part-time team members, interns, and contractors.

### Members
A member is an active participant in the ongoing activities of an TMRG. As a global company, the ways that members participate may vary based on location, culture, and preferences. Membership is open to both team members who identify with the diversity dimension that is the community’s focus and allies who wish to advocate and support the mission of the TMRG.

### Allies
An ally is someone who supports, empowers, or stands for another person or a group of people. Through our research, we have found it to be a best practice for all to be inclusive of ally support. When creating an TMRG, planning activities, and engaging with members be sure to consider how allies will be involved.

An ally strives to...
* be a friend
* be a listener
* be open-minded
* have their own opinions
* be willing to talk
* recognize their personal boundaries
* join others with a common purpose
* believe that all persons regardless of age, sex, race, religion, ethnicity, sexual orientation, gender identity and gender expression should be treated with dignity and respect
* recognize when to refer an individual to additional resources
confront their own prejudices
* recognize their mistakes, but not use them as an excuse for inaction
be responsible for empowering their role in a community
* commit themselves to personal growth in spite of the discomfort it may sometimes cause

As important as it is to define what an ally is in a positive way, it is also helpful to understand the boundaries of an ally's role.

An ally is NOT...
* someone with ready-made answers
* necessarily a counselor or trained to deal with crisis situations
expected to proceed with an interaction if levels of comfort or personal safety have been violated

[Adapted from Human Rights Campaign Establishing an Allies/Safe Zone Program, Human Rights Campaign](https://www.hrc.org/resources/establishing-an-allies-safe-zone-program)

Additional resources on how to be an ally:
* [Allyship at GitLab](https://about.gitlab.com/handbook/communication/ally-resources/)
* [Live Learning Ally Training at GitLab](https://ctb.ku.edu/en/table-of-contents/culture/cultural-competence/be-an-ally/main)
* [Chapter 27. Cultural Competence in a Multicultural World | Section 5. Learning to be an Ally for People from Diverse Gro…
Straight for Equality](https://ctb.ku.edu/en/table-of-contents/culture/cultural-competence/be-an-ally/main)
* [Guide to Allyship](http://www.guidetoallyship.com/#the-work-of-allyship)

## TMRG LEADERS

### Required of all TMRG leaders:
* Manager support to commit the time and to use this leadership role as a professional development opportunity
* A minimum one-year commitment in the role but understanding this may change to less and can be more.
* Must be a full-time GitLab team members.

### Qualities of an TMRG leader:
* Interest, passion and time to devote their time to the TMRG
* Possesses facilitation, team-building, and collaboration skills
* Has a desire to build a presence for the TMRG within GitLab

## Roles Within the Group
Group Members - At GitLab we all contribute!  Everyone has an opportunity to lead and provide feedback within the group.

**Executive Sponsor** (optional but recommended)  - An executive GitLab team member who is responsible and accountable for strategic support of the group
Share accountability for the success of the DIB group
Participate as an active member of the DIB group
Share information about the DIB group activities with other leaders
Provide insight and guidance to DIB group as needed
Partner with TMRG leads on issues, concerns, and resource needs of the community
May provide additional budget

**Lead** - A GitLab team member who is responsible and accountable for strategic direction and operations of the TMRG
* Operational lead of the TMRG
* Meets w/ DIB Manager as needed but otherwise quarterly.  This may be more often as a new TMRG is forming to consult with DIB Manager for planning and execution.
* Responsible for submitting annual and or quarterly plans (Note: Format to be provided by DIB Manager)
* Along with Co-lead, serves as contact for any team, department, or other GitLab team member requesting partnership or education with the TMRG

**Co-Lead** - A GitLab team member who supports the Lead in the strategy and operations of the TMRG
* Serve as contact in the absence of the Lead
* Along with Lead, serves as contact for any team, department, or other GitLab team member requesting partnership with the TMRG

### Optional roles
While not required, we recommend establishing other leadership positions to ensure that the responsibilities of the Lead and Co-Lead remain realistic and success is achievable for the TMRG. Here are some example roles we recommend for each TMRG that reflect the 4 elements of focus listed above:

Leader of Professional Member Development: Activities that further the development of group members.

Leaders of Outreach/Business Development: Connecting with communities beyond GitLab

Leader of Awareness and Education: Raising awareness and educating all associates.

Leader of Talent Acquisition/Retention: Promoting, growing, and developing the TMRG as a whole.

Treasurer: managing the budget of the TMRG, working on necessary approvals internally and looking at the ROI of any events that take place.


### How TMRG leaders are selected
The election process should follow GitLab’s fiscal year calendar to ensure the roles are aligned to our strategy. Smaller or recently forming TMRGs may choose not to have a formal election if membership can easily determine leadership.

It is important to monitor the TMRGs size to recognize when it has grown too large for an informal election process. Larger TMRGs (50 members or more) should use a formal selection process with nominations of some kind, summaries of each candidate’s qualifications shared with TMRG members, votes taken on a set date, and vetting process etc as a suggestion but not required.


### TMRG leader terms of service
TMRG leaders are suggested to commit at least one year to their leadership role, with the option for less if a situation arises or more if the TMRG members at large would like.  This can also be set up as a rotation of 6 months as well.  The TMRG can decide.

### TMRG leader succession
Leadership succession is critical to sustaining TMRGs and keeping leaders energized. Ideally outgoing leaders will have and overlap with incoming new leaders by acting as advisors to ensure a seamless transition.

Research suggests developing the next generation of leaders for your TMRGs by looking for members who have taken smaller roles in heading up committees or organizing events; speak with them about their interests and encourage them to take on more visible roles within the TMRG.

## Tools for TMRG Leaders
These resources are here to help you effectively lead and grow an TMRG.

### Communications
Communication within TMRGs keeps members aware of, involved with, and supportive of the group’s direction and activities. TMRGs can use several inlets of communication tools outlined below to keep members informed about meeting times, structure, membership, and updates.

Communications resources
* Slack Channels
* Google Calendars
* Google Forms
* Email templates
* Invite templates

### Asynchronous Team Member Participation

As an all remote organisation, having sync meetings can be very difficult to engage all members of the TMRG. To increase participation, we should think differently about what participation looks like and what an active member looks like.

Active TMRG Member:

A team member who provides meaningful interactions with the TMRG through decision making, discussion participation or interactions. These need not be spoken or written but could be other avenues such as slack emojis to indicate support, participation in decisions via a poll etc.

This does not take away the need for synchronous meetings but allows everyone to contribute in the way they feel most comfortable and is inclusive of all geographics.

### Guidance/Suggestions for Encouraging Participation:

Use tools that work alongside sync meetings that encourage participation in the meetings.

- [Standuply](https://standuply.com/) is great for running an async meeting, you can add video, ask bespoke questions that may have arose in the sync meeting and get wider perspectives.

- [Polly](https://slack.com/apps/A04E6JX41-pollyhttps://slack.com/apps/A04E6JX41-polly) is great to get decisions made asynchronously where you want wider input than those who were unable to attend sync meetings and reach a consensus and proceed to action. There are templates available in [Polly](https://www.polly.ai/template) which can be useful tools.

- [Loom](https://slack.com/apps/A9G1TH4S2-loom) could be a great tool to provide a quick video update of meetings or activities happening within the TMRG.

- Create a thread in Slack of the key points from the meeting that can be discussed.

- Have a bias towards action BUT allow async members to participate before making the decision. Have a 24-48 hours window to reply before a decision is made.

- Have more async meetings than sync meetings. Doing this will allow team members to feel as though they are not missing out on important discussions, as the discussions happen elsewhere more often.

**Examples:**

- Welcome to our mid-month slack meeting, here is a thread to discuss XYZ as voted for in Polly. Once the discussion has concluded, we will do another Polly to decide on any actions we wish to take.

- Standuply Questions: How do you feel since our last meeting? Is there anything you would like to discuss/get an opinion on related to the XYZ TMRG? Are there any actions/budget spend/sponsorships that you would like to see soon? Any news articles positive/negative that you would like to share?

- In the last synchronous meeting on xx/xx/xx some of the key things discussed were:
1)
2)
3)
You can see full agenda notes here: (insert google doc) feel free to add any further thoughts in this slack thread.

### Measure:

Keep a track of engagement across the different methods so you can understand where the most engagement happens. This can be very useful in determining what is best for your particular TMRG.

You can use this template which is fairly manual or choose your own methods.

### What to do if you’re asked to provide your opinion on behalf of GitLab
There may be times that you are asked to comment on the state of DIB at GitLab or your TMRG. When or if that happens, please contact/notify PR, Talent Brand and the DIB Manager.  Here are some general best practices that we share are helpful for all GitLab team members to know.
* Don't share information that hasn't already been disclosed publicly. This includes retention and turnover rates, associate demographics, compensation trends, hiring plans or numbers, headcount, new products, corporate strategy, and more.
* If you’re asked about our Diversity, Inclusion & Belonging  stats, refer to our [GitLab identity data](https://about.gitlab.com/company/culture/inclusion/identity-data/)
* Always remember that although we work in transparency, we want to me mindful of GitLab’s reputation and brand.

### TMRG Success measurement (Capturing Data)
Measuring the success of the TMRG is important for the sustainability of the group and for ensuring the group’s effectiveness.

Members of the TMRGs are encouraged to identify multiple ways the success will be tracked and measured over time. Here are some suggestions for measuring success:
* Reviews and reports on metrics should be submitted quarterly to the DIB Manager and updated to the handbook. We also encourage you to share metrics with the TMRG as a whole so there is a shared understanding of expectations, value added, and areas of improvement.
* Conduct an annual review with the DIB Manager, to discuss successes and opportunities for the upcoming year
* Consider reviewing and modifying, if needed, the TMRGs mission statement and/or goals (every year or less to move forward iterations with changes in the TMRG)
* Identify the right cadence of gathering member feedback via surveys, focus groups, or year-end review sessions. Don’t wait until there is a lack of engagement, ask early and often.

### Additional suggestions to measure success (may vary based on the state of the TMRG):
* Event attendance
* Number of events hosted
* Feedback from events
* Blog pages
* TMRG page etc
* Tracking TMRG membership numbers month to month
* Participation in the external community events
* Volunteer hours contributed by members of the TMRG
* Quantitative feedback from TMRG members
* Number of ally members (when appropriate to track)
* Google form list sign-ups from the handbook
* Communication methods in place (Slack, mailing list, calendar, etc)

### Resources for TMRGs
We have provided a number of optional resources for TMRGs to use that assist in setting the strategy, roadmap, financial planning etc.

* [TMRG Strategy Template](https://docs.google.com/presentation/d/1Zpz6z_0x3rbjYzUd6fSAojg8ME3FKfWPrI7cD4HmV48/edit?usp=sharing): This can be used to set a 6-12 month strategy for your TMRG, to create an action plan, execute and measure.
* [New TMRG Roadmap Template](https://docs.google.com/spreadsheets/d/19Qa6xLIaoxOBbPIoMjYTcL87lx1y1exGGB6z4iD64tY/edit?usp=sharing): This template gives some examples of a Roadmap for new or developing TMRGs
* [Estabished TMRG Roadmap Template](https://docs.google.com/spreadsheets/d/19Qa6xLIaoxOBbPIoMjYTcL87lx1y1exGGB6z4iD64tY/edit?usp=sharing): This template gives some examples of a yearly roadmap for established or mature stage TMRGs
* [Budget Tracker](https://docs.google.com/spreadsheets/d/1-BZrH1KsRBYl9r0EYEy6VXlKyvGGf585mwoj9Vz5rgA/edit?usp=sharing): This is a simple budget tracker for Lead or a treasurer of a TMRG to plan and keep a track of the annual budget given to TMRGs.

### Finance & Budgets for TMRGs

Each TMRG has a budget of $5000 per year (Subject To Change) to assist in the activities they wish to engage in to further develop the TMRG, Enable and Empower the members, develop activities & events or to buy merchandise.

This could include but not limited to:
* Paying a speaker to present at Gitlab
* Purchasing TMRG specific Swag
* External Consultants to help problem solve on TMRG issues
* Sponsoring a partnership with a external vendor
* Making a donation to a relevant charitable organization

We have developed a simple [Budget Tracking](https://docs.google.com/spreadsheets/d/1-BZrH1KsRBYl9r0EYEy6VXlKyvGGf585mwoj9Vz5rgA/edit?usp=sharing)tool to track the budget and finances of the TMRG, feel free to use this when planning the activities for the TMRG throughout the year.

## Key Milestones for TMRGs (Suggested)

### Develop membership via:
- DIB Monthly Call
- Slack Channels
whats-happening-at-gitlab
diversity_inclusion_and_belonging
Location specific channels such as APAC, loc_london_uk etc
Other TMRGs channels
Any other channels that seem relevant

### Hold your first call:

Discuss the 4 Pillars of the TMRGs
Discuss the mission of the TMRG
Discuss any immediate actions the TMRG could take
Discuss the cadence of the meetings

### Set up regular cadence of the calls and Async Participation Opportunities

Monthly is suggested - try to include timezones either by rotating or having more than one call

### Suggestion: Assign sub-leads for each pillar

To better execute and ensure that the Leadership duties of the TMRG are not overly burdensome on 1-2 individuals. It is suggested to add 1 or 2 leads to each pillar, the TMRG Lead or Co-lead can also co lead pillars.

### Develop a 6 month strategy

Using the TMRG Strategy Template or a derivative of the template. Develop a strategy and plan for the TMRG to help take steps towards achieving the Mission and a Vision.

This would be a great opportunity to include your Executive Sponsor

### Suggestion: Gain leadership sponsors

In addition to executive sponsorship, some of our TMRGs have found gaining Director+ sponsors very beneficial in the advancement of there TMRG, MIT TMRG is a great example of this.

### Hold your first All Gitlab event/session

A great way to gain traction and have an initial goal. Is to develop a All Gitlab event. Check out the Coming Out Day session from the Pride TMRG.

Develop a working group and figure out how you want to do this. Ideas could include but not limited to:

- Roundtables
- External Speakers
- Panel Sessions

**Suggested process for your event**

1. Contact your DIB Partner with the idea and formulate with them on the format that you would like it to take.
1. Source and confirm speakers whether internally or externally, set the date & time, taking into account budget considerations, regions etc.
1. DIB Partner and TMRG Lead will work with the People Experience team in the #people-connect Slack channel to schedule the event on the Team Meetings calendars.
1. Post the event to the appropriate Slack Channels.

### Execute strategy

### Re-Strategize every 6-12 months




