---
layout: markdown_page
title: "IBM APM"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary
IBM Application Performance Management (APM) monitors, analyzes and manages applications and IT infrastructure. It monitors and analyzes application performance, issues, root causes, and outages to improve user experience and stability.  It allows DevOps teams to monitor performance, availability and response times of applications allowing them to identify, isolate and diagnose any existing issues or potential problems.
      
GitLab has a powerful monitoring capability with Prometheus, a time-series monitoring service, providing a flexible platform for monitoring GitLab and other software products. GitLab provides out of the box monitoring with Prometheus, providing easy access to high quality time-series monitoring of GitLab services.  GitLab has built-in monitoring that can automatically monitor your deployed applications, with extensive support for native cloud, container and microservices technology.  Additionally, Gitlab uses Jaeger, an open source end-to-end distributed tracing system used for monitoring and troubleshooting microservices-based distributed systems.

## Resources
* [IBM APM](https://www.ibm.com/us-en/marketplace/application-performance-management)
